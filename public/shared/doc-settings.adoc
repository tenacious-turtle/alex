:doctype: book
:experimental:
:reproducible:
:icons: font
:listing-caption: figure
:sectnums:
:imagesdir: ../shared/images
:stylesdir: ../shared/styles
:toc: left
ifeval::["{backend}" == "html5"]
:toc-title: Tango Docs
endif::[]
ifdef::backend-pdf[]
:toc-title: Table of Contents
endif::[]
:toclevels: 2
:source-highlighter: prettify
:linkattrs:
:specialchars:
:doc-info: shared
:docinfodir: ../shared
:back-cover-image: image:back-cover.pdf[]
ifdef::backend-pdf[]
:title-logo-image: image:tango_logo.jpg[pdfwidth=4.25in,align=right]
:source-highlighter: rouge
endif::[]
