// Condition to only include rev history in the print version of the guide.
ifdef::backend-pdf[]
[preface]
== Revision History

[cols="1,2,4"]
|===
|Revision |Date |Comments

|`{revnumber}`
| {revdate}
| {revremark}

// after every uprev, add the previous under the most recent as so:
// v1.4 (next line) date (next line) remark

|===

endif::[]