// this guide has one attribute associated with the build (co) - this is handled within the build command with the doc lead; however if you need to rebuild this guide not using the build command, you will need to run the following in order to get the CO version and the customer version to build correctly and properly link to the web portal. That command is: $ build -a co && mv master.html master-co.html && buildpdf -a co && mv master.pdf master-co.pdf && build && buildpdf
= Policy Event Consumer Guide
// revision settings
:revnumber: Sample release
:revdate: {localdate}
:revremark: for sampling
:version-label!:
include::../shared/header.adoc[]
include::./pdf-rev.adoc[]
include::./local-variables.adoc[]

//HTML condition to offer chance to print PDF
ifeval::["{backend}" == "html5"]
[preface]
== Print PDF

ifndef::co[]
TIP: In case you wanted to kick it old school, you can link:master.pdf[Print the PDF, window="_blank"].
endif::[]

ifdef::co[]
TIP: In case you wanted to kick it old school, you can link:master-co.pdf[Print the PDF, window="_blank"].
endif::[]

endif::[]

== Overview

This guide explains what the *Policy Event Consumer* (PEC) is at a high level while also providing information on Call Detail Records (CDRs), application configuration, and reference information related to the PEC.

NOTE: As always, if you have any queries at all with this document, send {email-docs} a line.

// included modules

include::../{topicDir}/content-topics/policy-content/event-consumer/_pec-solution-overview.adoc[leveloffset=+1]

include::../{topicDir}/cdr-topics/policy-cdrs/event-consumer-cdrs/_policy-event-consumer-cdr.adoc[leveloffset=+1]

include::../{topicDir}/config-topics/policy-config/event-consumer-config/_pec-application-properties.adoc[leveloffset=+1]

// the following topics are for CO only - the build command will create two outputs - one including the following topics (master-co) and one without, the standard master.html/pdf files are for customers.

ifdef::co[]

include::../{topicDir}/config-topics/rabbit-config/_spcm-exchange-setup.adoc[leveloffset=+1]

{sp} +

include::../{topicDir}/config-topics/rabbit-config/_rabbit-configuration-for-pec.adoc[leveloffset=+1]

endif::[]