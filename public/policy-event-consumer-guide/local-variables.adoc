:guide: Policy Event Consumer Guide
:guide-slug: robi-policy-cdr-guide
:pcrf-variable: true
:pcrf-standard-samples: true
:pcrf-bics-samples: false
// :for-co: false
:modularized: true
:context: pec-user-guide
