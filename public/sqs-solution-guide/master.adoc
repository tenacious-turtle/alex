:context: understand-sqs
= SQS Solution Guide
// revision settings
:revnumber: Sample Release
:revdate: {localdate}
:revremark: for sampling
:version-label!:
include::../shared/header.adoc[]
include::./pdf-rev.adoc[]
include::./local-variables.adoc[]

// HTML condition to offer chance to print PDF
ifeval::["{backend}" == "html5"]
[preface]
== Print PDF

TIP: In case you wanted to kick it old school, you can link:master.pdf[Print the PDF, window="_blank"].

endif::[]

== Overview

This guide explains what the *Shared Quota Service* (SQS) is at a high level while also providing information on Call Detail Records (CDRs), API operations, application configuration, and reference information related to the SQS.

NOTE: As always, if you have any queries at all with this document, send {email-docs} a line.

// included modules

// need to add information from subscriber and operator point-of-view; no need to create separate documents, just ensure that both subscriber and operator perspectives are covered.

include::../{topicDir}/content-topics/sqs/_sqs-overview.adoc[leveloffset=+1]

include::../{topicDir}/content-topics/sqs/_sqs-subscriber-interaction.adoc[leveloffset=+1]

include::../{topicDir}/content-topics/sqs/_sqs-ussd-flow-sample.adoc[leveloffset=+2]

include::../{topicDir}/instruction-topics/sqs/_setup-the-sqs.adoc[leveloffset=+1]


== API operations

This section includes all API operations for the SQS. The SQS solution allows API operations to be performed for group creating, sharing data, managing group members, and setting recurring donations from a particular donor.

// standard API overview (with operation and visual)

include::../{topicDir}/api-topics/general-api/_standard-api-overview.adoc[leveloffset=+2]

// operations below
[[share-quota-api-operation]]
include::../{topicDir}/api-topics/sqs-api/_share-quota.adoc[leveloffset=+2]

include::../{topicDir}/api-topics/sqs-api/_share-quota-with-group.adoc[leveloffset=+2]

include::../{topicDir}/api-topics/sqs-api/_get-shareable-plan.adoc[leveloffset=+2]

include::../{topicDir}/api-topics/sqs-api/groups/_create-group.adoc[leveloffset=+2]

include::../{topicDir}/api-topics/sqs-api/groups/_get-group.adoc[leveloffset=+2]

include::../{topicDir}/api-topics/sqs-api/groups/_update-group.adoc[leveloffset=+2]

include::../{topicDir}/api-topics/sqs-api/groups/_delete-group.adoc[leveloffset=+2]

include::../{topicDir}/api-topics/sqs-api/members/_add-group-member.adoc[leveloffset=+2]

include::../{topicDir}/api-topics/sqs-api/members/_delete-group-member.adoc[leveloffset=+2]

include::../{topicDir}/api-topics/sqs-api/members/_get-group-member.adoc[leveloffset=+2]

include::../{topicDir}/api-topics/sqs-api/rec-donations/_create-rec-donation.adoc[leveloffset=+2]

include::../{topicDir}/api-topics/sqs-api/rec-donations/_get-rec-donation.adoc[leveloffset=+2]

include::../{topicDir}/api-topics/sqs-api/rec-donations/_get-all-rec-donations.adoc[leveloffset=+2]

include::../{topicDir}/api-topics/sqs-api/rec-donations/_delete-rec-donation.adoc[leveloffset=+2]

// SQS health chedck API operation for CO only
ifeval::["{for-co}" == "true"]
include::../{topicDir}/api-topics/sqs-api/_sqs-health-check.adoc[leveloffset=+2]
endif::[]

== CDRs
[[sqs-cdr-chapter-start]]
The Shared Quota Service solution generates CDRs when donations, recurring donations, and group management takes place. This section details those CDRs.

// included modules below

include::../{topicDir}/cdr-topics/sqs-cdrs/_sqs-donation-cdr.adoc[leveloffset=+2]

include::../{topicDir}/cdr-topics/sqs-cdrs/_sqs-recurring-donation-cdr.adoc[leveloffset=+2]

include::../{topicDir}/cdr-topics/sqs-cdrs/_sqs-group-cdr.adoc[leveloffset=+2]

// SQS application properties for CO only
ifeval::["{for-co}" == "true"]
[appendix]
include::../{topicDir}/config-topics/sqs-config/_sqs-application-properties.adoc[leveloffset=+1]
endif::[]

[appendix]
include::../{topicDir}/api-topics/spcm-api/plan-definitions/_plan-definition-type-json-key-value-descriptions.adoc[leveloffset=+1]
