Password expiry is disabled by default for each account. If you want to enable password expiry for an account, you will need to tick the *Password Expiry Enabled* checkbox in the Tango GUI under:
==========
*User Mgmt* > *Users*

image:./gui/ims-password-expiry-enabled-checkbox.jpg[]
==========


=== Where is this configured?

This feature is configured in the IMS `application.properties` file. You can specify the expiry value using the property below which is in milliseconds.

NOTE: Upon reaching the set expiry time, users will need to change their password on the *change password screen*.

[source%autofit, java]
----
# Sets the expiry times on passwords. (Default 180 days)
password.expiry.milliseconds = 15552000000
----

{sp} +

=== Setting a minimum expiry

You can also set a minimum expiry for passwords so that users cannot change their passwords quickly and bypass the <<Configure Password History Check, Password History Check>>.

To set the minimum expiry, use the `password.min-expiry.milliseconds` property to your desired value:

NOTE: The value is set in milliseconds and the default is 1 day. To disable the property, set the value to `0`.

[source%autofit, java]
----
# Sets the minimum expiry on passwords, i.e the amount of time that must pass between change password requests. (Default 1 day)
password.min-expiry-milliseconds = 86400000
----


