.Password Policy Rules
[cols="1,3"]
|===
|Rule | Description

|*White space*
| Determines if a password is allowed to have white space.

|*Password length*
| Determines if a password is within the desired length (when enabled). 

|*Dictionary substring*
| Determines if a password contains words that are not allowed (as defined in the blacklist).

|*Lower case*
a| Determines if a password contains the required number of lowercase characters.

NOTE: lowercase

|*Upper case*
a| Determines if a password contains the required number of uppercase characters.

NOTE: Uppercase

|*Special characters*
a| Determines if a password contains the required number of special characters.

NOTE: `%!+` (special characters)

|*Digit characters*
a| Determines if a password contains the required number of digit characters.

NOTE: `1838` (digit characters)

|*Minimum characteristics*
a| Determines if a password contains the required number of character rules.

NOTE: (i.e., 1 uppercase, unlimited lowercase, 1 special character, and 1 digit = ``Password!!2019``)

|===