= NAC runtime configuration files
[[nac-runtime-config-file-{context}]]

The NAC runtime configuration depends on the settings deployed in both the Captive Portal's `application.properties`
and file the HRG's `application.properies` file. Typically, these files are managed by link:mailto:support@tangotelecom.com[Tango support]. However, in some cases, it might be useful for you to make changes to these files. See the sample files below for a better understanding of each of the `application.properties` files.

ifeval::["{backend}" == "html5"]

* link:./files/cp-application.properties[Captive Portal application properties file, window="_blank"]
* link:./files/hrg-application.properties[HTTP Resource Generator application properties file, window="_blank"]

IMPORTANT: These files are samples and not unique to your deployment.

endif::[]


ifdef::backend-pdf[]

* xref:hrg-properties-file-{context}[HRG properties sample file]
* xref:cp-properties-file-{context}[Captive Portal properties sample file]

IMPORTANT: These files are samples and not unique to your deployment.

endif::[]

{sp} +
