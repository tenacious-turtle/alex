= SDGW syslog configuration

In order to configure the **Sponsored Data Syslog Server**, you'll need to setup a few things in order to get things moving. This topic guides you through this configuration.

ifdef::backend-pdf[]
It contains the following sections:

* <<Core configuration>>
* <<Set the API method>>
* <<Set API response timeout and HTTPs>>
* <<Additional configuration>>

endif::[]

. Enter the following in `route.cfg`:
[source%autofit,shell]
----
<server>/sponsordata-syslog-server/0 = 13901
----

[start=2]
. Add the following process to `PM.cfg`:

[source%autofit,properties]
----
[Process {N}]
    Description         =  Sponsored Data Syslog Server
    Process Path        =  /tango/bin/launchSDSYSLOG.sh
    Program Name        =  sponsordata-syslog-server
    Class Name          =  sponsordata-syslog-server/0/pmClient/0
    Port                =  13901
    Argument            =  -c /tango/config/sponsorDataSyslogServer_properties
    Logical Host        =  SDSYSLOG Host
    Primary Host        =  localhost
    Launcy Delay        =  10
    Initial Response    =  60
    Restart Attempts    =  3
    Polling Interval    =  2
    Outstanding Polls   =  10
----

[start=3]
. Add the following to the `CDR.cfg`:

[source%autofit,shell]
----
filename<X>  = active_syslog.cdr, /sdsyslog/sponsordata_sd_cdr.%Y%m%d_%H%M%S, sponsordata-syslog-server/0
----

{sp} +

== Set the API method

In the `sponsorDataSyslogServer.properties` file:

[source%autofit,properties]
----
sponsordata.syslogserver.sds.api.http.method.get=true
----

== Set API response timeout and HTTPs

In the `sponsorDataSyslogServer.properties` file:

[source%autofit,properties]
----
sponsordata.syslogserver.http.client.timeout.milliseconds=5000
sponsordata.syslogserver.http.client.https.cert.verify=false
----

== Additional configuration

Check out the full link:./files/sponsorDataSyslogServer.properties[Syslog Server Properties File, window="_blank"] file as of {localdate}.