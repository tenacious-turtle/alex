= NAC in the Tango DRE

The NAC is fully integrated into the *Tango Data Retail Engine (DRE)*, of which is detailed xref:nac-in-dre-visual-{context}[below].

The NAC also interfaces with externals systems such as Online Charging Systems (OCS) where initial redirection forces a subscriber to opt-in in order to avail of using the internet, which is reported to and confirmed by the OCS. The NAC can also query external Subscriber Profile Repositories (SPR) to retrieve subscriber information and settings if necessary.

=========
[[nac-in-dre-visual-{context}]]

<1> The V-SGSN routes subscriber data sessions through the GTP proxy. If the subscriber does not have an active data plan, the V-SGSN redirects the session to the NAC where the subscriber lands in the xref:captive-portal-quick-overview-{context}[Captive Portal].

<2> The NAC interfaces with the Tango Policy system (PCC) by adding new subscribers to the system, retrieving data plans, and executing subscriber-driven plan operations such as terminating a plan or activating a plans; actions that can be taken by the subscriber in the xref:captive-portal-quick-overview-{context}[Captive Portal].

<3> If reports are part of your Tango solution, the NAC sends feedback to the Tango's Real Time Engagement (RTE) component to enable reports on campaign uptake and other parameters as set out in your deployment.
=========

{sp} +

image:./nac/nac-in-dre.jpg[width="750" align="center"]

{sp} +
