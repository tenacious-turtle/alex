= Captive Portal overview
[[captive-portal-quick-overview-{context}]]

image::./nac/nac-cp-zoom.jpg[width="500" align="left"]

The principle component of the NAC is the *Captive Portal (CP)*. The Captive Portal is the landing page where subscribers find themselves upon roaming into your network. They access this via a URL received in an SMS or through being redirected when trying to browse the internet. Subscribers can:

* opt-in for data roaming
* view and purchase data plans and offers
* view and manage their current data plans

NOTE: Currently, the Captive Portal is available in English, Spanish, and Portuguese.

{sp} +
