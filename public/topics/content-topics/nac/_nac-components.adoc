= NAC components

The NAC consists of *six primary components* that work together with other Tango DRE features and a user management setup (often a SQL database) to deliver a seamless roaming experience for the subscriber. 

NOTE: Multi-tenancy is supported (if needed or desired) through a single NAC instance whereby each _tenant_ has a separately-configured captive portal reflecting their brand and identity.

image:./nac/nac-components-visual.jpg[width="750" align="center"]

<1> xref:captive-portal-quick-overview-{context}[Captive Portal]
<2> xref:uns-overview-{context}[User Notification Service]
<3> xref:hrg-overview-{context}[HTTP Resource Generator]
<4> xref:pmi-overview-{context}[Platform Management Interface]
<5> xref:ussd-orchestrator-overview-{context}[Orchestrator]
<6> xref:ims-overview-{context}[Identity Management Service]

{sp} +

// sub topics below

include::./_captive-portal-quick-overview.adoc[leveloffset=+1]

include::../uns/_uns-overview.adoc[leveloffset=+1]

include::./_hrg-overview.adoc[leveloffset=+1]

include::../pmi/_pmi-overview.adoc[leveloffset=+1]

include::../ussd-orchestrator/_ussd-orchestrator-overview.adoc[leveloffset=+1]

include::../ims-content/_what-is-ims.adoc[leveloffset=+1]
