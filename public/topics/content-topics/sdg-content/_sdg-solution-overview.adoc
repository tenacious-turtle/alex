= SDGW solution overview

The **Sponsor Data Gateway** or **SDGW** enables mobile network operators to promote data usage while at the same time third-party sponsors can promote their brand, goods or services. In doing so operators can realise a new revenue stream while subscribers benefit from subsidized or even free data usage.

IMPORTANT: Free access to self-care and free downloads of Apps can also be handled using sponsored data.

//condition for pdfs only whereby the following sections are displayed - nav assistance 
ifdef::backend-pdf[]
*Guide Sections*:

* <<The API>>
* <<Solution Architecture>>
* <<User Data Management>>

endif::[]

{sp} +

== The API

The solution enables business layers (e.g. Mavenir SDS) to __add__, __activate__, and __deactivate__ sponsored data campaigns at the network layer. Additionally, the SDGW enables reporting of sponsored campaign usage (__i.e., data usage on sponsored campaigns__) from the network layer back to the business layer. The solution consists of a software solution provided by **Tango Telecom** that is integrated to the Oracle PCC layer with cooperation and assistance from link:https://www.oracle.com/index.html[Oracle, window="_blank"].


== Solution Architecture


ifeval::["{backend}" == "html5"]
.Solution Architecture Diagram
image:./sdg/sdg-arch.jpg[]
endif::[]

ifdef::backend-pdf[]
.Solution Architecture Diagram
image:./sdg/sdg-arch.jpg[width="450", align="center"]
endif::[]

{sp} +

The SDGW solution provides an API endpoint for Sponsored Data Management or other external systems to be able to offer data free-of-charge or at a subsidized rate to subscribers.

The Tango platform receives a web service request from the client platform (**step 1**). Then, the Tango platform performs the operations necessary for the activation and de-activation of the promotion campaigns by sending XML structured requests to a web service served running on the OCSG (**step 2**). In turn, the OCSG distributes the activation/de-activation requests to the PCRF’s (**step 3**).

ifeval::["{backend}" == "html5"]
.SDGW Solution Steps Diagram
image:./sdg/steps.jpg[]
endif::[]

ifdef::backend-pdf[]
.SDGW Solution Steps Diagram
image:./sdg/steps.jpg[width="450", align="center"]
endif::[]

== User Data Management

The primary phase solution also includes specialised handling of the data usage reports provided by the Oracle OCSG. The image below outlines the architecture, highlighting the sub-component that handles usage reports, the **Usage Coordination Function** (UCF).

ifeval::["{backend}" == "html5"]
.User Data Management Diagram
image:./sdg/sdg-user-placeholder.jpg[]
endif::[]

ifdef::backend-pdf[]
.User Data Management Diagram
image:./sdg/sdg-user-placeholder.jpg[width="450", align="center"]
endif::[]

{sp} +

The **UCF** provides the basic functionality needed to deliver selected usage reports to the __Mavenir SDS system__ over an online HTTP-based HTML/XML interface. The UCF is designed to deliver the following key business requirements: 

*	Integration with the external system (Mavenir SDS) over an HTLM/XML interface.
*	Integration with the OCSG over the proprietary Oracle usage reporting interface.
*	High-availability on the interface to the OCSG.
*	Processing (decoding & reformatting) of usage reports received from the OCSG (Oracle format) into an open HTML/XML based format for consumption by Aquto.
*	Transmission of HTML/XML-encoded usage reports to Aquto in online real-time mode.
*	High-availability with load balancing on the interface to Aquto.
*	Logging for all incoming and outgoing transactions. 
*	Monitoring of the health of the link to the Mavenir SDS system.
*	Detection and handling of transmission failures on the Mavenir SDS interface.
*	Toolset for re-processing usage reports that fail to transmit. 
*	Specialised support and handling for reports that fail to transmit during long-duration outages on the link to Mavenir SDS.  
