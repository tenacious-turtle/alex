= PEC Solution Overview

The Subscriber Policy Charging Manager (SPCM) sends out events that can be consumed by 3rd party applications and APIs. The PEC allows these 3rd parties to be notified of events (see <<Event Types>>) from the SPCM. These events are passed through the PEC via RabbitMQ and onto the external application/api.

====
An example might be that a particular application wants to know when a `PLAN_EXPIRED` event type has occured so the application can trigger a notification to offer a plan renewal.
====

====
IMPORTANT: This document (and the *PEC*) assumes that the SPCM and SPCM exchange are established prior to configuration of the PEC.

ifdef::co[]
NOTE: For more on this, see <<Set up SPCM Exchange>> and <<Configure RabbitMQ for the PEC>>.
endif::[]
====

ifdef::co[]
image::./pec/pec-overview.png[]
endif::[]

ifndef::co[]
image::./pec/pec-overview-customers.png[]
endif::[]

.*PEC Flow*
. The event comes in through the exchange from the SPCM.
. The event enters the ``event_process`` queue.
. The event is processed and a CDR is written.
. If there are no errors, it moves onto the external API. icon:circle[role="green"]
.. If there is an error, a CDR is written on the error and it is returned to RabbitMQ and sits in the ``event_retry`` queue for the configured amount of time. icon:circle[role="red"]

====
NOTE: You can configure how many retries an event will attempt from the ``event_retry`` queue by checking out `pec-max-retries` in the <<General Configuration, the configuration section>>.

NOTE: To account for the 3rd party application/api being unavailable, internal configuration is setup that utilizes *TTLs* and *Dead Letter Queues* in RabbitMQ that informs the system of unavailability.

ifdef::co[]
For CO staff, see <<Configure RabbitMQ for the PEC>> for details on the TTL/Dead Letter Queue RabbitMQ configuration.
endif::[]
====

// include event type information
include::./_pec-event-types.adoc[leveloffset=+1]
