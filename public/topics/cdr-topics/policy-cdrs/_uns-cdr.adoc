= UNS CDR

The *User Notification Service* (UNS) which interacts with the USSD SI over the SE interface to send USSD messages to mobile devices has CDRs that are generated and stored in the file: `/tango/data/cdr/uns.cdr`.

:uns-cdr: 0
[cols="1,2,6"]
|===
|Field |Name |Description

|{counter:uns-cdr}
|`Subscriber ID`
| The MSISDN of the subscriber in Tango format.

|{counter:uns-cdr}
|`Service ID`
| The Tango CDR Service ID. This is *91* for UNS CDRs.

|{counter:uns-cdr}
|`Transaction Type`
a| The CDR transaction type. This is *1* on a successful plan purchase.

ifndef::noTenant[]
|{counter:uns-cdr}
|`Tenant ID`
| {define-tenant-id}
endif::[]

|{counter:uns-cdr}
|`CDR Generation Date`
| The date the CDR was created. *Format*: `DD/MM/YY`.

|{counter:uns-cdr}
|`CDR Generation Time`
| The time the CDR was created. *Format*: `HH:MM:SS`.

|{counter:uns-cdr}
|`Source Address`
| The source address of the sending application.

|{counter:uns-cdr}
|`Sending User`
| The MSISDN of the subscriber sending the message.

|{counter:uns-cdr}
|`To`
| The MSISDN of the subscriber sending the message.

|{counter:uns-cdr}
|`Message`
a| The notification text.

NOTE: If the message contains a comma, the comma is rendered as `%2C`. If the message contains a percentage sign, the sign is rendered as `%25`.

|{counter:uns-cdr}
|`Expiry Date`
| The date the notification expires. *Format*: `DD/MM/YYYY`.

|{counter:uns-cdr}
|`Expiry Time`
| The time the notification expires. *Format*: `HH:MM:SS`.

|{counter:uns-cdr}
|`Connection ID`
|This ID identifies the SMPP connection.

|{counter:uns-cdr}
|`Sequence Number`
a| The sequence number used in the SMPP PDU sent over the given connection to the SMSC.

NOTE: This value wraps from `65535`.

|{counter:uns-cdr}
|`Part Number`
a| Indicates the CDR part.

NOTE: This only occurs when the message is segmented.

|{counter:uns-cdr}
|`Total Parts`
a| The total number of parts in the CDR.

NOTE: This only occurs when the message is segmented.

|{counter:uns-cdr}
|`Request Status`
a| The status of the submit request.

.possible values
* 0 = `request sent`
* 1 = `request not sent` or `no response received`
* 2 = `request response received`

NOTE: Reasons for values `1` and `2` can be seen in *Field 15*.

|{counter:uns-cdr}
|`Request Fail Reason`
a| The notification failure reason.

NOTE: See <<Request Fail Reasons>> for specific codes.

|{counter:uns-cdr}
|`Request Target`
a| Indicates where the notifications are sent.

.possible values
* 0 = `SMSC`
* 1 = `USSD Gateway`
* 2 = `HRG`

|===

== Sample
ifdef::noTenant[]
.UNS CDR Sample
[source%autofit,perl]
----

00041000060200000001010001,91,1,23/10/2018,12:13:21,0035361501900,0620001101,+0620001101,TEST_1MB_1DAY_FUP pack is purchased on Tue Oct 23 12:13:21 BDST 2018 at Tk1.15 and is not auto renewable and will expire on  Wed Oct 24 00:01:00 BDST 2018.,23/10/2018,13:13:21,5,119749,,,0,0,0

----
endif::[]
ifndef::noTenant[]
.UNS CDR Sample
[source%autofit,perl]
----

00041000060200000001010001,91,1,tenantb,23/10/2018,12:13:21,0035361501900,0620001101,+0620001101,TEST_1MB_1DAY_FUP pack is purchased on Tue Oct 23 12:13:21 BDST 2018 at Tk1.15 and is not auto renewable and will expire on  Wed Oct 24 00:01:00 BDST 2018.,23/10/2018,13:13:21,5,119749,,,0,0,0

----
endif::[]

== Request Fail Reasons

* <<Request Target 0 (SMSC) and 1 (USSD Gateway)>>
* <<Request Target 2 (HRG)>>

=== Request Target 0 (SMSC) and 1 (USSD Gateway)

[cols="1,3"]
|===
|Code |Description

|`0`
| No failure.

|`1`
| Throttled.

|`2`
| Encoding error.

|`3`
| No service available.

|`4`
| Blackout.

|`5`
| Date format error.

|`6`
| Integer out of range.

|`7`
| Timed out waiting for SMSC response.

|`8`
| String is wrong length.

|`9`
| Unsupported notification type.

|`10`
| Notification type set to `none`.

|`11`
| Service is temporarily overloaded.

|`12`
| SMPP DDU send error.

|`13`
| Service failure.

|`14`
| Timer Start Exception.

|`16`
| SE send failure.

|===

=== Request Target 2 (HRG)

When the Request Target is the HRG (2), the reason will be displayed as follows:

[source%autofit,xml]
----

“<hrg http response code>; <server http result code>;
<server result success code>; <server result failure code>”

----

[cols="2,4"]
|===
|String |Description

|`<hrg http response code>`
| Standard HTTP response code.

|`<server http result code>`
| Standard HTTP response code.

|`<server result success code>`
| The service the HRG returns.

|`<server result failure code>`
| The service the HRG returns.

|===

