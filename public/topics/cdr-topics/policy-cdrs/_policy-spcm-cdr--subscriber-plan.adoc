= SPCM subscriber plan CDR
[[spcm-sub-plan-cdr-{context}]]

The SPCM generates subscriber plan CDRs for events relating to subscriber plans such as plan purchases, plan activation or de-activation, and plan expiry.

*Subsections* include:

* xref:spcm-sub-plan-trans-types-{context}[SPCM subscriber plan transaction types]
* <<SPCM Subscriber Plan CDR Sample>>

:sub-plan-cdr: 0
[cols="1,2,6"]
|===
|Field |Name |Description

|{counter:sub-plan-cdr}
|`Subscriber ID`
| The MSISDN of the subscriber in Tango format.

|{counter:sub-plan-cdr}
|`Service ID`
| The Tango CDR Service ID. This is *39* for subscriber plan CDRs.

|{counter:sub-plan-cdr}
|`Transaction Type`
a| The CDR transaction type. This is *0* on a successful plan purchase.

TIP: For a complete list of transaction types, see <<SPCM Subscriber Plan Transaction Types>>.

ifndef::noTenant[]
|{counter:sub-plan-cdr}
|`Tenant Id`
|{define-tenant-id}
endif::[]

|{counter:sub-plan-cdr}
|`CDR Generation Date`
| The date the CDR was generated. *Format*: `DD/MM/YY`.

|{counter:sub-plan-cdr}
|`CDR Generation Time`
| The time the CDR was generated. *Format*: `hh:mm:ss`.

|{counter:sub-plan-cdr}
|`Payment Type`
a| The payment method used for the plan purchase.

.possible values
* `-1` = unknown
* `0` = prepaid
* `1` = postpaid

|{counter:sub-plan-cdr}
|`Cost`
a| The cost of a purchased plan.

NOTE: This should only be set for transaction type 0 (successful plan purchase).

|{counter:sub-plan-cdr}
|`Plan Name`
| The name of the subscriber's plan.

|{counter:sub-plan-cdr}
|`Plan Version`
| The version of the plan. The value of the field is a positive integer only.

|{counter:sub-plan-cdr}
|`Plan ID`
a| The plan instance ID for the subscriber plan.

NOTE: This can be a shared or individual plan depending on the `Shared Plan Flag` field.

TIP: The field is empty if inapplicable.

|{counter:sub-plan-cdr}
|`Plan Type`
a| The plan type.

.possible values
* `0` = core plan
* `1` = add-on

|{counter:sub-plan-cdr}
|`Plan Classification`
| The plan definition classification such as `DATA`.

|{counter:sub-plan-cdr}
|`Shared Plan Flag`
a| A flag that indicates if a plan is shared.

.possible values
* `0` = not a shared plan
* `1` = shared plan

|{counter:sub-plan-cdr}
|`Max Occurence Count`
a| Indicates the number of times the plan is allowed to recur.

NOTE: If set to `0` or if the fiels is *empty*, there is no limit on plan recurrence.

|{counter:sub-plan-cdr}
|`Plan Precedence`
| The plan precedence value where `0` is the highest precendence.

|{counter:sub-plan-cdr}
|`Plan State`
a| The state of the subscriber's plan.

.possible values
* `0` = inactive
* `1` = charging
* `2` = purchased
* `3` = charging failed
* `4` = active
* `5` = expired
* `6` = charging error
* `7` = parked
* `8` = refunded
* `9` = refund deferred
* `10` = terminated

CAUTION: If the field is blank, it is likely the plan has expired and/or has been removed from the database.

|{counter:sub-plan-cdr}
|`Plan Purchase Source`
a| An indicator of the plan purchase source as provided by the SPCM web-service client.

===================================================
.format
`<sourceType>-<sourceInfo>`
===================================================

===================================================
.example
`pmi-admin` = using PMI username as source info
===================================================

NOTE: This field is empty if not provided in the plan purchase.


|{counter:sub-plan-cdr}
|`Plan Purchase Date`
a| The date of the plan purchase. *Format*: `DD/MM/YYYY`.

TIP: For recurring plans, this is the latest plan purchase date.

|{counter:sub-plan-cdr}
|`Plan Purchase Time`
a| The time of the plan purchase. *Format*: `hh:mm:ss`.

TIP: For recurring plans, this is the latest plan purchase time.

|{counter:sub-plan-cdr}
|`Plan Activation Date`
a| The date the plan was activated. *Format*: `DD/MM/YYYY`.

NOTE: For recurring plans, this is the latest plan activation date.

TIP: The field is blank for transaction type `0`.

|{counter:sub-plan-cdr}
|`Plan Activation Time`
a| The time the plan was activated. *Format*: `hh:mm:ss`.

NOTE: For recurring plans, this is the latest plan activation time.

TIP: The field is blank for transaction type `0`.

|{counter:sub-plan-cdr}
|`Plan Expiry Date`
| The date the plan expires. *Format*: `DD/MM/YYYY`.

|{counter:sub-plan-cdr}
|`Plan Expiry Time`
a| The time the plan expired. Format*: `hh:mm:ss`.

NOTE: The field is empty if the plan expiry is unlimited.

NOTE: The field is empty for transaction type `0`.

|{counter:sub-plan-cdr}
|`Plan Deactivation Count`
| The number of times the plan has been deactivated.

|{counter:sub-plan-cdr}
|`Plan Recurrence Flag`
a| The state of plan recurrence.

.values
* `0` = not recurring
* `1` = recurring

|{counter:sub-plan-cdr}
|`Plan Recurrence Count`
| The number of times the plan has recurred.

|{counter:sub-plan-cdr}
|`Plan Metering Type`
a| The plan metering type.

.values
* `0` = volume
* `1` = time

|{counter:sub-plan-cdr}
|`Plan Limit`
a| The maximum number of bytes or seconds of usage allowed for the plan. The `Plan Metering Type` field determines if this is in bytes or seconds.

NOTE: The field displays `-1` if not applicable.

|{counter:sub-plan-cdr}
|`Plan Purchase Type`
a| The type of plan purchase.

.possible values
* `0` = normal plan purchase.
* `1` = plan renewal (recurring renew plan)
* `2` = plan accumulation (repurchase of existing plan - rolled to new instance)
* `3` = pro-rata plan (applies to subscribers with fixed billing dates)

NOTE: The field is empty if inapplicable.

|{counter:sub-plan-cdr}
a|`Plan Limit Adjustments`
a| The plan limit adjustment due to a plan purchase, renewal, or top-up.

NOTE: The value here depends on the `Plan Purchase Type` field.

.values
* If type `0`, this field is also `0`.
* If type `1`, this field displays the rolled over usage amount.
* If type `2`, this field displays the plan limit increment due to accumulation.
* If type `3`, this field display the pro-rata adjustment amount.

TIP: If the `Transaction Type` is one of the following (19, 20, 21, 22), the field specifies the amount of the volume/validity booster. Check out <<SPCM Subscriber Plan Transaction Types>> for more details.


|{counter:sub-plan-cdr}
|`Discount Type`
a| The type of discount applied to the plan.

.options
* `0` = none
* `1` = fixed (e.g., meter rate from SPCM)
* `2` = dynamic (e.g., rate from DPS)

|{counter:sub-plan-cdr}
|`Cancellation Cause`
a| Explains why the plan was cancelled.

.values
* empty = plan not cancelled.
* `0` = cancelled by subscriber.
* `1` = cancelled because the renewal date changed for that plan.
* `2` = cancelled because the validity of the plan was synchronised with a new plan purchase.

|{counter:sub-plan-cdr}
|`Number Validity Boosters`
| Specifies the number of validity boosters applied to the plan
instance.

|{counter:sub-plan-cdr}
|`Number Volume Boosters`
| Specifies the number of volume boosters applied to the plan
instance.

|{counter:sub-plan-cdr}
|`Booster Source`
| The source of the booster.

|{counter:sub-plan-cdr}
|`Booster Cost`
| The cost of the booster.

|{counter:sub-plan-cdr}
|`Plan Cost`
a| The plan cost excluding VAT.

NOTE: This is only present when the transaction type is `0`.

|{counter:sub-plan-cdr}
|`VAT Rate`
a| The VAT rate.

NOTE: This is only present when the transaction type is `0`.

|{counter:sub-plan-cdr}
|`Chargeable Subscriber`
a| Denotes which subscribers may be charged.

.values
* `0` = if the subscriber was not charged for the plan.
* `1` = if the subscriber was charged for the plan.

TIP: This will always be `1` if not a shared plan.

NOTE: For shared plans, this will be `1` for the chargeable subscriber and 0 for other group members. This is only present when the transaction type is `0`.

|{counter:sub-plan-cdr}
|`Purchase Tags`
a| Displays the purchase tags by name and value.

====================================================================
.format
`name1=value1;name2=value2`
====================================================================

NOTE: This is only present when the transaction type is `0` or `1` - if tags were supplied.

TIP: Commas, equal signs, and semi-colons are percent-encoded if present in the name/value.

|{counter:sub-plan-cdr}
|`Loan Amount`
| Specifies the loan amount in the plan purchase request.

|{counter:sub-plan-cdr}
|`Validity Carry Forward Count`
| The count of the number of times a validity carry forward has occurred.

|{counter:sub-plan-cdr}
|`IMSI`
| The subscriber's IMSI.

|{counter:sub-plan-cdr}
|`Unit Amount`
a| The plan purchase unit amount. This overrides the plan definition unit amount (which is in bytes).

NOTE: This field is optional and only used if set in the plan purchase.

|{counter:sub-plan-cdr}
|`Validity Period`
a| The plan purchase validity period. This overrides the plan definition validity period.

NOTE: This field is optional and only used if set in the plan purchase.

|{counter:sub-plan-cdr}
|`Transaction ID`
| Specifies a unique transaction ID generated by the SPCM when it purchases a plan and sends the direct debit request to the OCI server.

|{counter:sub-plan-cdr}
|`Charging Decision`
a| Indicates whether charging is enabled for a plan. This field is optional and only present when the corresponding `charging` parameter is part of the `plan purchase request` API payload.

.values
* `0` = no charging; the plan definition is not chargeable
* `1` = no charging; SPCM charging is *disabled*
* `2` = no charging; plan instance charging is *disabled*
* `3` = charging; SPCM charging is *enabled*
* `4` = charging; plan instance charging is *enabled*

|===

// include trans types

include::_policy-spcm-cdr--subscriber-plan--subscriber-plan-trans-types.adoc[leveloffset=+1]

== SPCM Subscriber Plan CDR Sample
ifndef::noTenant[]
.SPCM Subscriber Plan sample
[source%autofit,perl]
----

000410010203040506070801,39,0,VodafoneIE,05/11/2013,11:30:59,2,2300,PlanA,,1,0,,3,4,05/11/2013,11:30:59,05/11/2013,11:30:59,05/11/2013,11:35:59,0,0,0,0,2147483648,-1,,2

----
endif::[]
ifdef::noTenant[]
.SPCM Subscriber Plan sample
[source%autofit,perl]
----

000410010203040506070801,39,0,05/11/2013,11:30:59,2,2300,PlanA,,1,0,,3,4,05/11/2013,11:30:59,05/11/2013,11:30:59,05/11/2013,11:35:59,0,0,0,0,2147483648,-1,,2

----
endif::[]
