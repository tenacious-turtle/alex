= Notification SMS CDR
[[lbo-notification-sms-cdr-{context}]]

The Notification SMS CDR contains details of the “Submit-SM” message we send back to the inbound roamer at the end of the session. 

The following table describes the fields in this CDR type (47,5). 

:notsms-field: 0
[cols="1,2,6"]
|===
|Field |Name |Description

|`{counter: notsms-field}`
|`Subscriber ID`
|{define-subscriber-id}

|`{counter: notsms-field}`
|`Service ID`
|The Tango service ID. This should be set to *47* for LBO SMS provisioning.

|`{counter: notsms-field}`
|`Transaction Type`
a|The CDR transation type; possible values are:
* `5` = Notification SMS CDR  

ifndef::noTenant[]
|`{counter: notsms-field}`
|`Tenant ID`
| {define-tenant-id}
endif::[]

|`{counter: notsms-field}`
|`Date stamp`
|The date on which the CDR was produced in the format [yyyy-mm-dd].

|`{counter: notsms-field}`
|`TIme stamp`
a|The time at which the CDR was produced in the format [hh:mm:ss].

|===

NOTE: Fields (1) through (6) may not always be present depending on the CDR handler configuration; Refer to  the section on the CDR handler. 
[cols="1,2,6"]
|===
|Field |Name |Description

|`{counter: notsms-field}`
|`CDR Correlation ID`
|A string to be used for all CDRs associated with this session.NOTE: This field is 9 characters in length.

|`{counter: notsms-field}`
|`Time elapsed`
|The time (in milliseconds) from when we the submit-SM message was sent until response is received. 

|`{counter: notsms-field}`
|`Valid response received`
a|Indicates whether a valid ForwardSM-Response message was received. 

.possible values
* `1` = a valid forwardSM-Response message was received 
* `0` = a valid forwardSM-Response message was not received

|`{counter: notsms-field}`
|`Cause of no valid response`
a|An integer error code returned to the SL. 

.possible values
* `0` = OK
* `1` = Message not sent due to internal error
* `2` = Message not sent due to invalid parameters
* `3` = Message not sent due to unavailability of external resource
* `4` = Message not sent due to configuration
* `10` = Timed out internally while awaiting response
* `11` = Timed out while awaiting response from network
* `20` = Stack unable to decode response
* `30` = GSM MAP error - System Failure [reported from network]
* `31` = GSM MAP error - Unknown Subscriber [reported from network]
* `32` = GSM MAP error - Absent Subscriber [reported from network]
* `49` = GSM MAP error other than those in range 30-32

NOTE: This is only set if “Valid response received” is set to 0.

|`{counter: notsms-field}`
|`MAP “User Error”`
a|The value of the “User Error” field from the the MAP FSM response message.

NOTE: This field is only set if a GSM MAP error was returned from the network.

|`{counter: notsms-field}`
|`SC Address`
a|Identifies the SMSC to which the message is submitted.

NOTE: This field may be empty if the destination SC Address was not determined.

|`{counter: notsms-field}`
|`Orig GT`
|The Global Title used when sending the message; this is equivalent to the sender MSISDN.

|`{counter: notsms-field}`
|`Number of octets in message content`
a|The number of bytes used to express the encoded message content.  

NOTE: This may be different to the number of characters in the message content, since it may be packed according to 7-bit encoding, or it could be encoded using UCS2, with 2 octets per character.

|`{counter: notsms-field}`
|`Data Coding Scheme`
|The data coding scheme according to SMS specification.

|===

== Sample

The following is an example of a Notification SMS CDR: 

[source%autofit]
----
0004100301060401060000030402,47,5,2014-08-27,09:32:23,BlEMeY6Ui,27,1,,,3532222,3531122334455,55,0
----
