= SPCM subscriber profile CDR update causes
[[sub-profile-cdr-update-types-{context}]]
These are the update causes for the SPCM subscriber profile CDR.

[cols="2,8"]
|===
|Cause |Description

|`-1`
| not applicable

|`0`
| plan activation

|`1`
| plan deactivation

|`2`
| dynamic profile enabled (plan usage rule violation)

|`3`
| plan expiry

|`4`
| usage consumed

|`10`
| usage available

|`11`
| plan terminated

|`12`
| dynamic profile disabled (plan usage rule activation)

|`13`
| plan activation pending

|`14`
| booster applied (validity or volume booster)

|===
