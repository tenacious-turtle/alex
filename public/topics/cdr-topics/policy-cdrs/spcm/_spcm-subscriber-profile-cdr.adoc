:context: spcm-sub-profile-cdr
= SPCM subscriber profile CDR
[[spcm-sub-profile-cdr]]

The SPCM generates a *subscriber profile CDR* when subscriber profiles are created or updated. The CDR contains a fixed part and a variable part.

Fixed part::
The fixed part contains information about the subscriber and a plan if relevant.

Variable part::
The variable part contains usage information parent plan and plan precedence. As a plan may have multiple counters, the variable part produce in each CDR provides details of the _current status_ of each counter at the time the CDR was produced.

IMPORTANT: The variable part of the CDR has a terminating element of `0;0;0;0`. Each field in the variable part is prepended with a `&` and are separated by a `;`.

.fixed part
:spcm-sub-profile-cdr: 0
[cols="1,3,6"]
|===
|Field |Name |Description

|{counter:spcm-sub-profile-cdr}
|`subscriber ID`
a| {define-msisdn}

|{counter:spcm-sub-profile-cdr}
|`service ID`
a| The Tango CDR Service ID. This is *40* for subscriber profile CDRs.

|{counter:spcm-sub-profile-cdr}
|`transaction type`
a| The CDR transaction type. This is *0* on a subscriber profile creation and *1* for a subscriber profile update.

ifndef::noTenant[]
|{counter:spcm-sub-profile-cdr}
|`Tenant Id`
|{define-tenant-id}
endif::[]

|{counter:spcm-sub-profile-cdr}
|`update cause`
a| The cause of the update.

NOTE: For a complete list of update causes, see xref:sub-profile-cdr-update-types-{context}[subscriber profile update causes].

|{counter:spcm-sub-profile-cdr}
|`CDR generation date`
a| The date the CDR was generated. *Format*: `DD/MM/YY`.

|{counter:spcm-sub-profile-cdr}
|`CDR generation time`
a| The time the CDR was generated. *Format*: `hh:mm:ss`.

|{counter:spcm-sub-profile-cdr}
|`plan name`
a| The name of the plan that trigged the subscriber profile update.

NOTE: This field is empty if not applicable (_profile creation)_.

|{counter:spcm-sub-profile-cdr}
|`plan precedence`
a| The precedence of the plan that triggered the subscriber profile update.

NOTE: If not applicable, you see `-1` (_profile creation)_. 

|{counter:spcm-sub-profile-cdr}
|`rule name`
a| The rule name if the update was caused by a rule violation.

NOTE: This field is empty if the update was not caused by a rule violation.

|{counter:spcm-sub-profile-cdr}
|`IMSI`
a| The subscriber's IMSI.

|===

.variable part
[cols="1,4,5"]
|===
|Field |Name |Description

|{counter:spcm-sub-profile-cdr}
|`parent plan name`
| The name of the parent plan of the PCC profile.

|{counter:spcm-sub-profile-cdr}
|`parent plan precedence`
a| The precedence value for the parent plan of the PCC profile.

|{counter:spcm-sub-profile-cdr}
|`pcc profile name`
| The name of the PCC profile.

|{counter:spcm-sub-profile-cdr}
|`pcc profile precedence`
| The precedence value for the PCC profile.

|===

include::./_spcm-cdr-subscriber-profile-cause-types.adoc[leveloffset=+1]

