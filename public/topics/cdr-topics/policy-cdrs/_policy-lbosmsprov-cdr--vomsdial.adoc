= VoMs Dialog CDR
[[lbo-voms-dialog-cdr-{context}]]

The VoMs Dialog CDR contains details of the MML Request and response. The following table describes the fields in this CDR type (47,3).

:vomsdial-field: 0
[cols="1,2,6"]
|===
|Field |Name |Description

|`{counter: vomsdial-field}`
|`Subscriber ID`
|{define-subscriber-id}

|`{counter: vomsdial-field}`
|`Service ID`
|The Tango service ID. This should be set to 47 for LBO SMS provisioning.

|`{counter: vomsdial-field}`
|`Transaction Type`
a|The CDR transation type.

.possible values
* `3` = VoMs Dialogue CDR  

ifndef::noTenant[]
|`{counter: vomsdial-field}`
|`Tenant ID`
| {define-tenant-id}
endif::[]

|`{counter: vomsdial-field}`
|`Date stamp`
|The date on which the CDR was produced in the format [yyyy-mm-dd].

|`{counter: vomsdial-field}`
|`Time stamp`
|The time at which the CDR was produced in the format [hh:mm:ss].

|===

NOTE: Fields (1) through (6) may not always be present depending on the CDR handler configuration; Refer to  the section on the CDR handler. 
[cols="1,2,6"]
|===
|Field |Name |Description

|`{counter: vomsdial-field}`
|`CDR Correlation ID`
a|A string to be used for all CDRs associated with this session.

NOTE: This field is 9 characters in length.

|`{counter: vomsdial-field}`
|`Total Time elapsed including queue`
|The time (in milliseconds) from when CHGTRIG request is sent into the queue of outgoing messages until response is received.  

|`{counter: vomsdial-field}`
|`Response Latency`
|The time (in milliseconds) from when CHGTRIG request was actually sent out on the MML/TCP interface, until response is received. 

|`{counter: vomsdial-field}`
|`IP address of VOMS server`
|The IP address of the VoMs server. This field may be empty in the event that the IP address could not be determined due to an error.

|`{counter: vomsdial-field}`
|`Port of VOMS server used`
|The port address of the VoMs server. This field may be empty in the event that the port could not bedetermined due to an error.

|`{counter: vomsdial-field}`
|`Error with response`
a|A flag indicating whether a valid response was received from VOMS.

.possible values
* `0` = means that a valid response was received
* `1` = means that no valid response was received

NOTE: If this field shows that a valid response was received, that does not guarantee that the recharge operation was successful.

|`{counter: vomsdial-field}`
|`MSISDN`
a|The MSISDN in the format used in the request.

NOTE: This is not related to the MSISDN of the inbound roamer, this is a ‘fake’ value which is assigned uniquely for the LBO SMS Provisioning service to use when interacting with VOMS.

|`{counter: vomsdial-field}`
|`CARDPIN`
|The ‘voucher number’ used in the request

|`{counter: vomsdial-field}`
|`RETN`
|The ‘return code’ in the response.  

|`{counter: vomsdial-field}`
|`FACEVALUE`
a|The value of the ‘FACEVALUE’ attribute returned in the responsemessage from VOMS.  

NOTE: This field will be empty if the ``FACEVALUE`` attribute is not present in the response.

|===

== Sample

The following is an example of a VoMs Dialog CDR: 

[source%autofit]
----
0004100301060401060000030402,47,3,2014-08-27,09:32:23,BlEMeY6Ui,10,0,127.0.0.1,12000,0,+353861000001,12345678910,0,55
----
