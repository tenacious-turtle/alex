= SPCM web service audit log CDR
[[spcm-web-audit-cdr-{context}]]

The *Web Service Audit Log* is a type of CDR that records access to the SPCM web service endpoints. The following formats are supported:

* `REQUEST` logs each request received.
* `AUTHENTICATION` logs the authentication information for the user.

NOTE: The log file is found at `/tango/logs/spcm/spcm-rest-ws-audit.log`.

*Subsections* include:

* <<Request Log>>
* <<Authentication Log>>
* <<Web Service Audit Log CDR Samples>>

== Request Log

:spcm-web-request: 0
[cols="1,2,6"]
|===
|Field |Name |Description

|{counter:spcm-web-request}
|`Audit Type`
| The type of audit log. This is `REQUEST` for the request log.

|{counter:spcm-web-request}
|`Date`
| The date of the web service request. *Format*:`DD/MM/YYYY`.

|{counter:spcm-web-request}
|`Time`
| The time of the web service request. *Format*: `hh:mm:ss`.

ifndef::noTenant[]
|{counter:spcm-web-request}
|`Tenant ID`
|{define-tenant-id}
endif::[]

|{counter:spcm-web-request}
|`ID`
| The unique ID that links `REQUEST` and `AUTHENTICATION` audit log entires.

|{counter:spcm-web-request}
|`IP Address`
a| The IP address from where the request originated.

NOTE: This is `http <x>-FORWARDED-FOR` if it exists on the request.

|{counter:spcm-web-request}
|`HTTP Method`
a| The http method.

.options
* `GET`
* `PUT`
* `POST`
* `DELETE`

|{counter:spcm-web-request}
|`URL`
| The web service endpoint that is being accessed.

|{counter:spcm-web-request}
|`User`
| The user specified in the HTTP basic authorisation header.

|{counter:spcm-web-request}
|`SPCM Tango User`
a| This is an HTTP header that is used to identify the originating subscriber if the request comes through the USSD channel.

NOTE: This is used by the Groups API to grant appropriate access to a subscriber based on group membership.

===================================
TIP: Requests from 3rd party clients can set this field to the following.

[source%autofit,perl]
----
*
----
===================================

|{counter:spcm-web-request}
|`Custom Header`
| Contains the value of the optional header that can be set on the HTTP request.

|===

== Authentication Log

:spcm-web-auth: 0
[cols="1,2,6"]
|===
|Field |Name |Description

|{counter:spcm-web-auth}
|`Audit Type`
| The type of audit log. This is `AUTHENTICATION` for the authentication log.

|{counter:spcm-web-auth}
|`Date`
| The date of the web service request. *Format*:`DD/MM/YYYY`.

|{counter:spcm-web-auth}
|`Time`
| The time of the web service request. *Format*: `hh:mm:ss`.

ifndef::noTenant[]
|{counter:spcm-web-auth}
|`Tenant ID`
|{define-tenant-id}
endif::[]

|{counter:spcm-web-auth}
|`ID`
| The unique ID that links `REQUEST` and `AUTHENTICATION` audit log entires.

|{counter:spcm-web-auth}
|`IP Address`
a| The IP address from where the request originated.

NOTE: This is `http <x>-FORWARDED-FOR` if it exists on the request.

|{counter:spcm-web-auth}
|`User`
| The user specified in the HTTP basic authorisation header.

|{counter:spcm-web-auth}
|`Security Enabled`
a| Flag that indicates if authentication is enabled.

.values
* `0` = disabled
* `1` = enabled

|{counter:spcm-web-auth}
|`Authenticated`
a| Flag that indicates if a user has been authenticated or not.

.values
* `0` = user not authenticated
* `1` = user authenticated

|===

== Web Service Audit Log CDR Samples
ifndef::noTenant[]
.Request Sample
[source%autofit,perl,subs=attributes]
----

REQUEST,15/03/2016,16:10:26,VodafoneID,86,127.0.0.1,GET,/spcm-restws/pcc/spcm/subscribers/{fakeNumber}/plans/55262/usage,joe,*,

----

.Authentication Sample
[source%autofit,perl]
----

AUTHENTICATION,15/03/2016,16:10:26,VodafoneIE,86,joe,0,0

----
endif::[]

ifdef::noTenant[]
.Request Sample
[source%autofit,perl]
----

REQUEST,15/03/2016,16:10:26,86,127.0.0.1,GET,/spcm-restws/pcc/spcm/subscribers/0876827150/plans/55262/usage,joe,*,

----

.Authentication Sample
[source%autofit,perl]
----

AUTHENTICATION,15/03/2016,16:10:26,86,joe,0,0

----
endif::[]
