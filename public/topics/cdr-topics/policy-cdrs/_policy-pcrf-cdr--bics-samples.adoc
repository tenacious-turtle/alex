= PCRF CDR samples

This topic shows you PCRF samples including:

* <<PCRF Start Session>>
* <<PCRF Interim Rule>>
* <<PCRF Stop Session>>


{sp} +

== PCRF Start Session
.PCRF start sample
[source%autofit,perl]
----

0004100107030403080606090007,28,1,3166,0,-1,-2,0,0,1,1,09/08/2019,11:29:47,0,-2,,357332094836174,x.ispsn,5,1004,194.158.50.156,,,,27821FFFEFF,0,-1,,,gtpproxy4.us.bics.com;1564727481;180052;4005112F@GX-gtpproxy_logic_4-,,,0,11:29:47,0,11:29:47,-1,0,0,0,0,0,0,-1,-1,-1,-1,-1,8001,,,,,,312530002262410,,,,,,,,,,,,,,,,,,,,,,,

----

== PCRF Interim Rule
.PCRF interim-rule sample
[source%autofit,perl]
----

0004100107030403080606090007,28,3,3166,0,-1,-2,0,0,1,1,09/08/2019,11:29:47,1,-2,,357332094836174,x.ispsn,5,1004,194.158.50.156,,,,27821FFFEFF,0,-1,,,gtpproxy4.us.bics.com;1564727481;180052;4005112F@GX-gtpproxy_logic_4-,,,0,11:29:47,0,11:29:47,0,0,0,0,0,0,0,-1,-1,-1,-1,1,8001,,,,,,312530002262410,,,,SPRINT_DEFAULT_128KBPS,0,1,0,GSM_Device_Default,-1,-1,-1,-1,-1,-1,23,24431879-1,50000008,0,-1,0,0,0,0

----

== PCRF Stop Session
.PCRF stop sample
[source%autofit,perl]
----

0004100107030403080606090007,28,2,3166,0,-1,0,0,0,1,1,09/08/2019,11:29:49,3,-2,22.96.218.13,357332094836174,x.ispsn,5,1004,194.158.50.156,,,,27821FFFEFF,3,-1,,,gtpproxy4.us.bics.com;1564727481;180052;4005112F@GX-gtpproxy_logic_4-,,,0,11:29:47,2,11:29:47,10,2,0,0,0,0,0,-1,-1,-1,-1,1,8001,,,,,,312530002262410,,,,SPRINT_DEFAULT_128KBPS,0,1,2,GSM_Device_Default,-1,-1,-1,-1,-1,-1,-1,24431879-1,50000008,0,104,0,0,0,0

----
