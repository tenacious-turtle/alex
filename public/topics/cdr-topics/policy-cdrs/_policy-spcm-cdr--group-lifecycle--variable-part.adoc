= SPCM group lifecycle CDR variable part
[[spcm-group-lifecycle-variable-part-{context}]]

The Group Lifecycle CDR also has a variable part. The variable part contains the group members' information.

NOTE: If there is no data to record, only the terminating element will be displayed.

====================================================================
It contains zero or more elements and a terminating element.

IMPORTANT: Each field in the variable part is separated by a semi-colon (;) whereas the elements are separated by an ampersand (&).

IMPORTANT: The terminating element is `(0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0;0)`.
====================================================================

[cols="1,2"]
|===
|Field |Description

|`Member MSISDN`
| The MSISDN of the group member subscriber.

|`Role`
| The role the member has in the group.

|`Member Default Usage Percentage`
| The default usage percentage for the member.

|`Member Default Policy Control Type`
a| The default policy control type for the member.

.values
* `0` = QoS control
* `1` = Service control

|`Member Default Notification Enabled`
a| Flag that indicates if notifications are enabled for this member.

.options
* `0` = false (disabled)
* `1` = true (enabled)

|`Member Plan Name`
| The name of the member's plan.

|`Member Plan Name Usage Percentage`
| The usage percentage for the specified member plan.

|`Member Plan Policy Control Type`
a| The policy control type for the specified member.

.values
* `0` = QoS control
* `1` = Service control

|`Member Plan Notification Enabled`
a| Flag that indicates if notifications are enabled for the specified plan.

.options
* `0` = false (disabled)
* `1` = true (enabled)

|===
