= SPCM subscriber plan CDR transaction types
[[spcm-sub-plan-trans-types-{context}]]

[cols="1,4"]
|===
|Type | Description

|`0`
a| Plan purchase successful.

NOTE: Celebrate good times, c'mon!

|`1`
| Plan purchased failed.

|`2`
| Plan activated.

|`3`
| Plan deactivated.

|`4`
| Usage consumed

|`5`
| Plan expiry.

|`6`
| Plan cancelled.

|`7`
| Plan provisioning failure.

|`8`
| Plan terminated.

|`9`
| Plan volume top-up.

|`10`
| Plan validity period top-up.

|`11`
| Plan top up time.

|`12`
| Usage available.

|`13`
| Plan renewal parked.

|`14`
| Plan refunded.

|`15`
| Plan refund deferred.

|`16`
| Not in use.

|`17`
a| Activation pending timeout.

NOTE: This represents plans with an activation mode of `FIRST_USAGE` that have exceeded the plan activatin timeout period when waiting for a usage report.

|`18`
a| Plan validity carried forward.

CAUTION: This transaction type is not created if the expiry timestamp of the current plan is after the newly-selected plan.

|`19`
| Plan volume has been boosted.

|`20`
| Plan validity has been boosted.

|`21`
| Failure to charge for the plan booster.

|`22`
a| The plan booster charged successfully. However, the application of the booster failed.

TIP: Bummer, man.

|`23`
| Plan usage was reset.

|`24`
| Shared quota was deducted

|`25`
| Shared quota was rolled back.

|`26`
| Shared quota was added.

|===
