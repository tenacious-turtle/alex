= OCI server CDR

The *Open Charging Interface* (OCI) Server CDRs are generated when a subscriber purchases a data plan using the PCC platform.

:oci-cdr-field: 0
[cols="1,2,6"]
|===
|Field |Name |Description

|{counter:oci-cdr-field}
|`Subscriber ID`
| The MSISDN of the subscriber.

|{counter:oci-cdr-field}
|`Service ID`
| The Tango Service ID. This is set to *30* for the OCI Server.

|{counter:oci-cdr-field}
|`Transaction Type`
a| The OCI transaction type.

.possible values
* 1 = `Charge Reservation`
* 2 = `Debit of Charge Reservation`
* 3 = `Direct Debit of a charge`
* 4 = `Direct Credit of a charge`
* 5 = `Loan request to Jhotpot interface`

ifndef::noTenant[]
|{counter:oci-cdr-field}
|`Tenant ID`
| {define-tenant-id}
endif::[]

|{counter:oci-cdr-field}
|`Result Code`
| If successful, this is *0*. Otherwise see <<OCI Server Result Codes>>.

|{counter:oci-cdr-field}
|`CDR Generation Date`
| The date the CDR was generated. *Format*: `DD/MM/YYYY`.

|{counter:oci-cdr-field}
|`CDR Generation Time`
| The time the CDR was generated. *Format*: `hh:mm:ss`.

|{counter:oci-cdr-field}
|`Payment Method`
a| The method of payment that was used.

.possible values
* -1 = `unknown`
* 0 = `postpaid`
* 1 = `prepaid`

|{counter:oci-cdr-field}
|`Session ID`
| The Tango Internal Charging Interface (ICI) session ID.

|{counter:oci-cdr-field}
|`IMSI`
| The IMSI of the subscriber who is charged.

|{counter:oci-cdr-field}
|`Location Info`
| Location information on the subscriber such as *roaming status*.

|{counter:oci-cdr-field}
|`Event Info`
a| Additional information regarding the event that triggered the payment such as *Plan Name*, *Plan ID*, and *Plan Booster* if applicable. The values are separated by a vertical delimiter.

NOTE: This field is empty if not provided by the client.

|{counter:oci-cdr-field}
|`VAT Rate`
a| The percentage added to the charge due to VAT.

NOTE: This is empty if the `unit type` field is not a monetary value. The value is *0* if VAT does not apply.

|{counter:oci-cdr-field}
|`Unit Type`
a| The unit type of the transaction.

.possible values
* 2 = `Volume (bytes`
* 3 = `Time (seconds)`
* 50 = `Money`

|{counter:oci-cdr-field}
|`Units`
| The number of units reserved or charged. This is the exact value received by the OCI client.

|{counter:oci-cdr-field}
|`Total Cost without Exponent`
| The value sent to the OCS after any VAT has been applied but *before* the `costExponent` has been applied.

|{counter:oci-cdr-field}
|`Loan`
| The amount charged to the subscriber's loan amount.

|{counter:oci-cdr-field}
|`Retry Count`
| The number of times a direct debit retry was attempted.

|{counter:oci-cdr-field}
|`Total Cost with Exponent`
| The value sent to the OCS after any VAT has been applied and *after* the `costExponent` has also been applied.

|===

== Sample
ifndef::noTenant[]
.OCI CDR Sample
[source%autofit,perl]
----

00041000080701020304050607,30,3,VodafoneIE,0,23/01/2018,12:20:35,1,0,1234567,,,50,100,100,0
----
endif::[]

ifdef::noTenant[]
.OCI CDR Sample
[source%autofit,perl]
----

00041000080701020304050607,30,3,0,23/01/2018,12:20:35,1,0,1234567,,,50,100,100,0
----
endif::[]

== OCI Server Result Codes

[cols="1,3"]
|===
|Code |Description

|`1`
| Protocol error detected by the charging gateway.

|`2`
| Protocol error detected by charging handler.

|`3`
| Release call issue by prepaid platform.

|`4`
| Subscriber has zero balance or balance has expired.

|`5`
| Unknown subscriber announcement received.

|`6`
a| Prompt and collect user information.

TIP: This is probably due to a new subscriber.

|`7`
| Protocol error detected in SINAP/INAP/CAP server.

|`8`
| No data usage for a defined period of time.

|`9`
| Subscriber barred announcement played before release.

|`10`
| Subscriber barred announcement played before release.

|`11`
| An undefined announcement played before release.

|`12`
| Timeout waiting for the prepaid platform.

|`13`
| Failue in looking up subscriber database.

|`14`
| Invalid B-number.

|`15`
| Tariff not found.

|`32`
| Date proxy close received.

|`33`
| Failure to find suitable rating entry.

|`34`
| Insufficient balance.

|`48`
| Reserve request amount exceeded.


|`49`
| Error in indicated volume parameter.


|`50`
| Error in indicated charging information parameter.


|`51`
| Charging interface configurably blocked.


|`64`
| Reservation has expired.


|`80`
| Undefined network error.


|`81`
| Undefined subscriber error.

|===
