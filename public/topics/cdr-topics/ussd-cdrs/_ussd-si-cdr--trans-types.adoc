= USSD SI CDR transaction types
[[ussd-cdr-trans-types-{context}]]

[cols="1,2,6"]
|===
|ID |Transaction Type |Description

|42
|`PROCESS_USSD_REQUEST`
a| A *process USSD request* was received by the user. It occurs at the beginning of every MI dialogue.

.contains
* Information about the content of the request message.
* Details on the service instruction which is carried out by the orchestrator.

|43
|`PROCESS_USSD_RESPONSE`
a| A *process USSD response* was sent to the user. This may occur at the end of an MI dialogue or immediately before a handover from MI to NI portions.

.contains
* Information about the content of the response message.
* Identity of the service instruction that was carried out.

|44
|`USER_REQUEST`
a| A *ussd request message* was sent to the user and the corresponding USSD response was received.

.contains
* Information about the content of both request and response messages.
* The identity of the service instruction that was carried out.
* Indicates success or failure of the operation.

CAUTION: This is also printed when the USSD response is not received because an error occurred while awaiting a response message.

|45
|`USER_NOTIFY`
a| A *ussd notify message* was sent to the user and an acknowledgement response was received by the handset.

.contains
* Information about the content of the notify messages.
* The identity of the service instruction that was carried out.
* Indicates success or failure of the operation.

CAUTION: This is also printed when the USSD response is not received because an error occurred while awaiting a response message.

|46
| `SERVER_REQUEST`
a| A *service request* was called upon.

.contains
* The identity of the service instruction that was carried out.
* Indicates success or failure of the operation.

|47
|`HANDOVER`
a| A *handover* from the MI portion of the dialogue to the NI portion of the dialogue.

.contains
* The identity of the service instruction that was carried out.

|48
|`SCRIPT`
a| A *javascript* has been interpreted.

.contains
* The identity of the service instruction that was carried out.
* Indicates success or failure of the operation.

|49
|`CONTENT`
a| A *text* was sent to or received by the user.

.contains
* The content of the text.

|===
