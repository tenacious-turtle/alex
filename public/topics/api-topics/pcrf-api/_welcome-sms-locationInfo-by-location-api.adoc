= WSMS API | Get location entity by location

This `GET` operation retrieves a *location information entity* by location.

===========================
[get]#*GET*# ``/pcc/pcrf/location/<name>/locationInfo``

*accept*: application/JSON

*permissions*: `PCRF_LOCATION_READ_PERMISSION`
===========================

== URL parameters

IMPORTANT: There is one *required* parameter in this operation.

[cols="20,60,20"]
|===
| Parameter | Description | Sample

|`name`
| The name of the location.
| `Tijuana`

|===

== Response

The response returns all location information entities and their associated parameters. An example is shown below:

[source%autofit, java]
----
"locationInfoList": [{	
	"locationInfo": {
		"id": 24,
		"name": "Tijuana",
		"description": null,
		"locationInfoType": {
			"id": 5,
			"name": "PLMNID",
			"value": 4
		},
		"locationInfo": "123000"
		},
	"_links": {
		"self": {
			"href": "http://localhost:8080/pcrf-ws/pcc/pcrf/locationInfos/Tijuana"
			},
		"location": {
			"href": "http://localhost:8080/pcrf-ws/pcc/pcrf/locations/Tijuana"
		}
	}
},

"locationInfo": {
		"id": 24,
		"name": "Tijuana-2",
		"description": null,
		"locationInfoType": {
			"id": 5,
			"name": "PLMNID",
			"value": 4
		},
		"locationInfo": "123000"
		},
	"_links": {
		"self": {
			"href": "http://localhost:8080/pcrf-ws/pcc/pcrf/locationInfos/Tijuana-2"
			},
		"location": {
			"href": "http://localhost:8080/pcrf-ws/pcc/pcrf/locations/Tijuana-2"
		}
	}
}]

----
