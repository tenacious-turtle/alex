= WSMS API | Get Location information entities

This `GET` operation retrieves location information entities.

===========================
[get]#*GET*# ``/pcc/pcrf/locationInfos``

*accept*: application/JSON

*permissions*: `PCRF_LOCATION_READ_PERMISSION`
===========================

== URL parameters

NOTE: To get a specific location information entity, use the `name` parameter at the end of the url.

[cols="20,80"]
|===
| Parameter | Description

|`name`
| The name of the location information entity.

|===

== Response

The response returns all location information entites and their associated parameters including `id`, `name`, `description`, and `locationInfoType`, `locationInfo`, and associated `links`. An example is shown below:

[source%autofit, java]
----
{
	"locationInfo": {
	    "id": 35,
	    "name": "testConor5",
	    "description": null,
	    "locationInfoType": {
		    "id": 4,
		    "name": "SGSN",
		    "value": 3
	},
	    "locationInfo": "100.100.100.206"
	},
	    "_links": {
		    "self": {
			    "href": "http://localhost:8080/pcrf-ws/pcc/pcrf/locationInfos/EU-west"
		}
	}
}
----
