= Plan type descriptions
[[plan-type-json-key-value-descriptions-{context}]]

[cols="2,1,3"]
|===
|Field |Type |Description

|`planDefinition`
| integer
a| The plan definition information for the plan.

IMPORTANT: See xref:plan-definition-type-json-key-value-descriptions-{context}[plan definition type information] more details on what to expect within the plan definition information you receive.

|`maxOccurenceCount`
| integer
a| Specifies the maximum number of times this plan can recur.

.options
* `0` = no limit
* `1` = does not auto-renew

NOTE: Any value from `2` and higher specifies the number of times the plan will recur.

|`id`
| string
| The unique identifier for the plan.

|`state`
| enum
a| The current state of the plan in its lifecycle.

.options
* `available` = unit metering type is available for the plan
* `consumed` = unit meter type is consumed for this plan

|`purchaseTimestamp`
| dateTime
a| The time at which the plan was purchased.

NOTE: Format is link:https://www.iso.org/iso-8601-date-and-time-format.html[ISO 8601, window="_blank"].

|`activationTimestamp`
| dateTime
a| The time at which the plan was activated.

NOTE: Format is link:https://www.iso.org/iso-8601-date-and-time-format.html[ISO 8601, window="_blank"].

|`expiryTimestamp`
| dateTime
a| The time at which the plan will expire.

NOTE: Format is link:https://www.iso.org/iso-8601-date-and-time-format.html[ISO 8601, window="_blank"].

|`updateTimestamp`
| dateTime
a| The time at which the plan was updated.

NOTE: Format is link:https://www.iso.org/iso-8601-date-and-time-format.html[ISO 8601, window="_blank"].

|`cancelled`
| boolean
a| Flag that indicates if the plan has been cancelled.

.options
* `true` = cancelled
* `false` = not cancelled

|`usage`
| long  
| The plan's current usage. The unit type is defined in `unitMeteringType` in the xref:plan-definition-type-json-key-value-descriptions-{context}[plan definition].

|`deactivationCount`
| short
| The number of times the plan has been deactivated.

|`purchaseSource`
| string
| Identifies the source of the plan purchase such as `PMI`.

|`allowedUnitAmount`
| string
| The current allowed plan amount (taken into account rollover, accumulation, and pro-rated data/validity).

|`occurrenceCount`
| integer
| The number of times the plan recurred.

|`usageState`
| enum
a| The current usage state of the plan.

.options
* `available` = unit metering type is available for the plan
* `consumed` = unit meter type is consumed for this plan

|`parkedTimestamp`
| dateTime
a| The time when the plan was parked for the first time during the current renewal.

NOTE: Format is link:https://www.iso.org/iso-8601-date-and-time-format.html[ISO 8601, window="_blank"].

|`parkedNextRetryTimestamp`
| dateTime
a| The time when the next retry will be made.

NOTE: Format is link:https://www.iso.org/iso-8601-date-and-time-format.html[ISO 8601, window="_blank"].

|`boosterVolumeCount`
| short
| The number of times the plan used the volume booster.

|`boosterValidityCount`
| short
| The number of times the plan used the validity booster.

|===
