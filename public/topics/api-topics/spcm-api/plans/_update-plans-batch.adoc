:context: update-plans-batch-api-operation
:api-object: subscriber
:api-op: postPut
= Update multiple plans

This `PUT` operation updates multiple subscriber plans. You pass the `msisdn` in the URL and the `planDefinitionName` and `operation` in the `JSON` payload. If successful, you receive an HTTP response code of `201` and a `JSON` response confirming with the `planId` which plans have been updated.

IMPORTANT: Currently, only the `terminate` operation is available for batch plan updates. 

NOTE: See <<http-response-codes-{context}, HTTP response codes>> for other response codes.
 
===========================
[put]#*PUT*# ``/pcc/spcm/subscribers/<msisdn>/plans/batch-update`` + <<update-plans-batch-json-payload, JSON request payload>>

*content-type*: application/json

*accept*: application/vnd.com.tango.pcc.{SPCMplanAPIversion}+json

*permissions*: `SPCM_PLAN_UPDATE_PERMISSION`
===========================

== URL parameters

There is one mandatory parameters.

[cols="1,1,3"]
|===
| Parameter | Type | Description

|`msisdn`
| string
| {define-msisdn}

|===


[[update-plans-batch-json-payload]]
== JSON request payload

The following example terminates the plans ``whoShotTheSheriff` and `itWasNotTheDeputy`.

[source,json]
----
{
    "items" :
        [
            {
                "planDefinitionName" : "whoShotTheSheriff",
                "operation" : "terminate"
            },
            {
                "planDefinitionName" : "itWasNotTheDeputy",
                "operation" : "terminate"
            }
}
----

== JSON response

The sample response to the above payload shows the `planId` of the plans that were terminated.

[source,json]
----
{
    "items" :
        [
            {
                "itemIndex" : 0,
                "planId" : 4200
            },
            {
                "itemIndex" : 1
                "planId" : 3958
            }
}
----

include::../../general-api/_http-response-codes.adoc[leveloffset=+1]
