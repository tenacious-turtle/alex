:context: delete-booster-api-operation
:api-object: booster
:api-op: getDelete
= Delete a booster

This `DELETE` operation removes a booster from your system by `id`. If successful, you receive an HTTP response code of `200`.

NOTE: See <<http-response-codes-{context}, HTTP response codes>> for other response codes.
 
===========================
[delete]#*DELETE*# ``/pcc/spcm/boosters/<id>``

*permissions*: `SPCM_PLAN_BOOSTER_DELETE_PERMISSION`
===========================

== URL parameters

[cols="1,1,3"]
|===
| Parameter | Type | Description

|`id`
| integer
| The unique identifier of the booster.

|===

include::../../general-api/_http-response-codes.adoc[leveloffset=+1]
