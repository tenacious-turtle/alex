:context: delete-pcc-profile-api-operation
:api-object: pcc profile
:api-op: getDelete
= Delete a PCC profile

This `DELETE` operation deletes a PCC profile by using the `planDefinitionID` and `pccProfileIDd` URL parameters. If successful, you receive an HTTP response code of `200`.

NOTE: See <<http-response-codes-{context}, HTTP response codes>> for other response codes.
 
===========================
[delete]#*DELETE*# ``/pcc/spcm/planDefinitions/<planDefinitionId>/pccProfiles/<pccProfileId>``

*accept*: application/hal+JSON

*permissions*: `SPCM_PLAN_DEFINITION_DELETE_PERMISSION`
===========================

[[delete-pcc-profile-url-parameters]]
== URL parameters

There are two URL parameters.

[cols="1,1,3"]
|===
| Parameter | Type | Description

|`planDefinitionId`
| integer
| {define-planDefinitionId}

|`pccProfileId`
| integer
| A unique identifier for the pcc profile.

|===

include::../../../general-api/_http-response-codes.adoc[leveloffset=+1]