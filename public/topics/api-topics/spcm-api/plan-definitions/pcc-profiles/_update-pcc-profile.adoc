:context: update-pcc-profile-api-operation
:api-object: pcc profile
:api-op: postPut
= Update a PCC profile

This `PUT` operation updates a PCC profile by using the `planDefinitionID` and `pccProfileId` URL parameters. If successful, you receive an HTTP response code of `200` and a `JSON` response containing the updated pcc profile information.

NOTE: See <<http-response-codes-{context}, HTTP response codes>> for other response codes.
 
===========================
[put]#*PUT*# ``/pcc/spcm/planDefinitions/<planDefinitionId>/pccProfiles/<pccProfileId>`` + <<update-pcc-profile-json-payload, JSON request payload>>

*content-type*: application/hal+JSON

*accept*: application/hal+JSON

*permissions*: `SPCM_PLAN_DEFINITION_CREATE_PERMISSION`
===========================

[[update-pcc-profile-url-parameters]]
== URL parameters

There are two URL parameters.

[cols="1,1,3"]
|===
| Parameter | Type | Description

|`planDefinitionId`
| integer
| {define-planDefinitionId}

|`pccProfileId`
| integer
| A unique identifier for the pcc profile.

|===

[[update-pcc-profile-json-payload]]
== JSON request payload

The following example updates a pcc profile's `alias` to `newAlias`. See <<pcc-profile-type-json-key-value-descriptions-{context}, PCC profile descriptions>> for all possible data types.

[source,json]
----
{
    "alias": "newAlias"
}
----

// include the key value description table
include::./_pcc-profile-type-json-key-value-descriptions.adoc[leveloffset=+2]

{sp} +

== JSON response

The following example shows the `JSON` response to a ``JSON`` request updating `alias` to `newAlias` with a `planDefinitionId` of `200` (shown as `_links` URL in the response) and a `pccProfileId` of `756` (shown as `id`) which were passed as <<update-pcc-profile-url-parameters, URL parameters>>. This particular sample returns only some of the data types possible. You may see different key:value pairs if your request is slightly different.

NOTE: See <<pcc-profile-type-json-key-value-descriptions-{context}, PCC profile descriptions>> for all possible data types.

[source,json]
----
{
    "id": 756,
    "alias": "newAlias",
    "qosProfileName": "QoS Default",
    "serviceProfileName": "Service Default",
    "networkProfileName": "NetworkProfile1",
    "deviceProfileName": null,
    "timeProfileName": "TimeProfile1",
    "locationProfileName": null,
    "chargingProfileName": null,
    "subscriptionProfileName": null,
    "threshold": false,
    "meteringPercentage": null,
    "precedence": 1,
    "_links": {
        "self": {
            "href": "http://localhost:8080/spcm-rest-ws/pcc/spcm/planDefinitions/
        200/pccProfiles/756"
        }
    }
}
----

// include the key value description table
// include::./_pcc-profile-type-json-key-value-descriptions.adoc[leveloffset=+2]

{sp} +

include::../../../general-api/_http-response-codes.adoc[leveloffset=+1]