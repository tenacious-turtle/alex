:context: delete-qos-profile
:api-object: qos profile
:api-op: getDelete
= Remove QoS control profile from plan definition

This `DELETE` operation removes the quality of service (QoS) control profile from a plan definition. If successful, you receive an HTTP response code of `204`.

NOTE: See <<http-response-codes-{context}, HTTP response codes>> for other response codes.
 
===========================
[delete]#*DELETE*# ``/pcc/spcm/planDefinitions/<planDefinitionId>/qosControlPccProfile``

*content-type*: application/JSON

*permissions*: `SPCM_PLAN_DEFINITION_DELETE_PERMISSION`
===========================

== URL parameters

There is one mandatory parameter.

[cols="1,1,3"]
|===
| Parameter | Type | Description

|`planDefinitionId`
| integer
| {define-planDefinitionId}

|===

{sp} +

include::../../../general-api/_http-response-codes.adoc[leveloffset=+1]
