:context: set-action-to-plan-def
:api-object: plan definition
:api-op: postPut
= Set action for a plan definition

This `PUT` operation sets an action for a plan definition. If successful, you receive an HTTP response code of `204`.

NOTE: See <<http-response-codes-{context}, HTTP response codes>> for other response codes.
 
===========================
[put]#*PUT*# ``/pcc/spcm/planDefinitions/<planDefinitionID>/action``

*permissions*: `SPCM_PLAN_DEFINITION_CREATE_PERMISSION`
===========================

== URL parameters

There is one mandatory parameter.

[cols="1,1,3"]
|===
| Parameter | Type | Description

|`planDefinitionId`
| integer
| {define-planDefinitionId}

|===

{sp} +

include::../../general-api/_http-response-codes.adoc[leveloffset=+1]
