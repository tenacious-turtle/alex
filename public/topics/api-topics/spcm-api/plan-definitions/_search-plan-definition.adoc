:context: search-plan-definition-api-operation
:api-object: plan definition
:api-op: getDelete
= Search for a plan definition

This `GET` operation searches for a plan definition using query parameters `name` and `current` as in the status of the plan definition. If successful, you receive an HTTP response code of `200` and a `JSON` response which includes all of the information for the requested plan definition.

NOTE: See <<http-response-codes-{context}, HTTP response codes>> for other response codes.
 
===========================
[get]#*GET*# ``/pcc/spcm/planDefinitions?name=<PlanName>&current=true``

*accept*: application/hal+JSON

*permissions*: `SPCM_PLAN_DEFINITION_READ_PERMISSION`
===========================

== URL query parameters

There are two possible query parameters.

NOTE: If you use both query parameters, only plan definitions matching both criteria will be returned.

[cols="1,1,3"]
|===
| Parameter | Type | Description

|`name`
| string (max 255)
| The name of the plan definition.

|`current`
| boolean
| Indicates if a plan definition is current or not where `true` means the plan definition is current and `false` means it is not.

|===

== JSON response

The following example shows the `JSON` response for a request for a plan with a `name` of `planDefinition01` (shown as `name` in the payload). This particular sample returns only some of the data types possible. You may see more when requesting other plan definitions.

NOTE: See <<plan-definition-type-json-key-value-descriptions-{context}, Plan definition descriptions>> for all possible data types.

[source,json]
----
{
    "id": 192,
    "name": "planDefinition01",
        "validityPeriod": {
            "validityPeriod": "1week"
    },
    "grantedAmount" : {
        "volumeAmount" : 1000
    },
    "unitMeteringType": "VOLUME",
    "core": false,
    "recurring": false,
    "cost" : 100,
    "unitAmount": "1000",
    "planPrecedence": 0
}
----

// include the key value description table
include::./_plan-definition-type-json-key-value-descriptions.adoc[leveloffset=+2]

{sp} +

include::../../general-api/_http-response-codes.adoc[leveloffset=+1]