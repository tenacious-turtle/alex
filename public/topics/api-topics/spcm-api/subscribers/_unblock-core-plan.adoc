:context: unblockCorePlan
:api-operator: subscriber
:api-op: getDelete
= Unblock subscriber's core plan

This `PUT` operation unblocks a subscriber's core plan by deactivating any rule violations that might be blocking it. If successful, you receive an HTTP response code of `200` and a small `JSON` success response.
 
===========================
[put]#*PUT*# ``/pcc/spcm/subscribers/<msisdn>/unblockCore``

*accept*: application/JSON

*permissions*: `SPCM_PLAN_UPDATE_PERMISSION`
===========================

== URL parameters

There is one mandatory parameter.

[cols="1,1,3"]
|===
| Parameter | Type | Description

|`msisdn`
| string (max 255)
| {define-msisdn}

|===

== Response

If you receive a `200` HTTP response code (valid subscriber), you will receive a `JSON` payload similar to the following:

NOTE: For other HTTP response codes, see <<http-response-codes-{context}, HTTP response codes>>.

[source,json]
----
{
    "status": "success"
}
----

{sp} +

include::../../general-api/_http-response-codes.adoc[leveloffset=+1]