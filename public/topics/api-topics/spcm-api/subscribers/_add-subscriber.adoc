:context: add-subscriber-api-operation
:api-object: subscriber
:api-op: postPut
= Add a new subscriber

This `POST` operation creates a new subscriber on your system. All of the information needed to create the subscriber is passed in a `JSON` payload and an HTTP response code of `201` is returned upon successul creation of the subscriber.

NOTE: See <<http-response-codes-{context}, HTTP response codes>> for other response codes.
 
===========================
[post]#*POST*# ``/pcc/spcm/subscribers`` + <<JSON request payload>>

*content-type*: application/JSON

*accept*: application/JSON

*permissions*: `SPCM_SUBSCRIBER_CREATE_PERMISSION`
===========================

== JSON request payload

[source,json]
----
{
    "msisdn": "0871234567",
    "imsi": "871234567",
    "alternateNotificationMsisdn": "0871234567",
    "paymentType": "postpaid",
    "class": "Standard",
    "locale": "en",
    "status": "active",
    "dpsEnabled": false,
    "dpsNotification": false,
    "eosNotification": true,
    "paygNotification": true,
    "imei": "01234567890129",
    "zone": "TEST_LZ_Mumbai",
    "renewalDayOfMonth": 15,
    "tag": "treatOftheWeek"
}
----

// include the key value description table
include::./_subscriber-type-json-key-value-descriptions.adoc[leveloffset=+2]

{sp} +

include::../../general-api/_http-response-codes.adoc[leveloffset=+1]