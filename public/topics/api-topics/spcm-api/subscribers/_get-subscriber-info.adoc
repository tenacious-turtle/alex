:context: get-sub-info-operation
:api-object: subscriber
:api-op: getDelete
= Get subscriber information

This `GET` operation retrieves information about a specific subscriber (_by MSISDN_). You receive a ``JSON`` response containing everything about the specified subscriber.
 
===========================
[get]#*GET*#  ``/pcc/spcm/subscribers/<msisdn>``

*accept*: application/JSON

*permissions*: `SPCM_SUBSCRIBER_READ_PERMISSION`
===========================

== URL parameters

There is one mandatory parameter.

[cols="1,1,3"]
|===
| Parameter | Type | Description

|`msisdn`
| string (max 255)
| {define-msisdn}

|===

== Response

If you receive a `200` HTTP response code (valid subscriber), you will receive a `JSON` payload similar to the following:

NOTE: For other HTTP response codes, see <<http-response-codes-{context}, HTTP response codes>>.

[source,json]
----
{
    "msisdn": "0871234567",
    "imsi": "871234567",
    "alternateNotificationMsisdn": "0871234567",
    "paymentType": "postpaid",
    "class": "Standard",
    "locale": "en",
    "status": "active",
    "dpsEnabled": false,
    "dpsNotification": false,
    "eosNotification": true,
    "paygNotification": true,
    "imei": "01234567890129",
    "zone": "TEST_LZ_Mumbai",
    "renewalDayOfMonth": 15,
    "tag": "limerickPonyClub"
}
----

// include the key value description table
include::./_subscriber-type-json-key-value-descriptions.adoc[leveloffset=+2]

include::../../general-api/_http-response-codes.adoc[leveloffset=+1]
