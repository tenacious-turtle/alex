:context: delete-a-subscriber-operation
:api-operator: subscriber
:api-op: getDelete
= Delete a subscriber

This `DELETE` operation deletes a specific subscriber (_based on MSISDN_) from the system. If successful, you receive a status code of `200`.

NOTE: See <<http-response-codes-{context}, HTTP response codes>> for other response codes.
 
===========================
[delete]#*DELETE*# ``/pcc/spcm/subscribers/<msisdn>``

*permissions*: `SPCM_SUBSCRIBER_DELETE_PERMISSION`
===========================

== URL parameters

There is one mandatory parameter.

[cols="1,1,3"]
|===
| Parameter | Type | Description

|`msisdn`
| string (max 255)
| {define-msisdn}

|===

include::../../general-api/_http-response-codes.adoc[leveloffset=+1]