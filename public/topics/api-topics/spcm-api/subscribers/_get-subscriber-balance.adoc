:context: get-sub-balance-api-operation
:api-operator: subscriber
:api-op: getDelete
= Get subscriber balance

This `GET` operation retrieves the subscriber's current balance (_by MSISDN_). If successful, an HTTP response code of `200` is returned as well as a ``JSON`` response containing the balance (and possible *loan* information if applicable).
 
===========================
[get]#*GET*# ``/pcc/spcm/subscribers/<msisdn>/balance``

*accept*: application/JSON

*permissions*: `SPCM_SUBSCRIBER_READ_PERMISSION`
===========================

== URL parameters

There is one mandatory parameter.

[cols="1,1,3"]
|===
| Parameter | Type | Description

|`msisdn`
| string (max 255)
| {define-msisdn}

|===

== Response

If you receive a `200` HTTP response code (valid subscriber), you will receive a `JSON` payload similar to the following:

NOTE: For other HTTP response codes, see <<http-response-codes-{context}, HTTP response codes>>.

[source,json]
----
{
    "balance": 775,
    "maxLoan": 50
}
----

=== JSON descriptions

[cols="1,3"]
|===
| Parameter | Description

|`balance`
| The remaining balance of the subscriber (in local currency).

|`maxLoan`
| The maximum loand currently available to lend to the subscriber.

|===

include::../../general-api/_http-response-codes.adoc[leveloffset=+1]