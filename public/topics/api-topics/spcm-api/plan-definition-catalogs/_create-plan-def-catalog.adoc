:context: create-plan-def-catalog-definition-api-operation
:api-op: postPut
= Create a new plan definition catalog

This `POST` operation creates a new plan definition catalog on the system. If successful, you receive an HTTP response code of `201` as well as a `JSON` response body showing the information passed in the <<create-plan-def-catalog-json-payload, JSON request payload>> as well as an `id` assigned to the new plan definition.

NOTE: See <<create-plan-def-catalog-http-response-codes, HTTP response codes>> for other response codes.
 
===========================
[post]#*POST*# ``/pcc/spcm/planDefinitionCatalogs`` + <<create-plan-def-catalog-json-payload, JSON request payload>>

*content-type*: application/JSON

*accept*: application/hal+JSON

*permissions*: `SPCM_CATALOG_CREATE_PERMISSION`
===========================

[[create-plan-def-catalog-json-payload]]
== JSON request payload

The following example shows only the mandatory attributes for creating a new plan definition catalog. See <<plan-definition-catalog-type-json-key-value-descriptions-{context}, Plan definition catalog descriptions>> for all possible data types.

[source,json]
----
{
    "name": "IBM Catalog",
    "description": "All IBM Plans",
    "attributes": [
        [
        "enterpriseID",
        "IBM"
        ],
        [
        "owner",
        "admin"
        ]
    ],
    "names": [
        "Plan001",
        "Plan002",
        "Plan003"
        ]
}
----

// include the key value description table
include::./_plan-definition-catalog-type-json-key-value-descriptions.adoc[leveloffset=+2]

== JSON response

The following shows the response for <<create-plan-def-catalog-json-payload, the above sample>>. Note the `id` being added.

[source,json]
----
{
    "id": 42,
    "name": "IBM Catalog",
    "description": "All IBM Plans",
    "attributes": [
        [
        "enterpriseID",
        "IBM"
        ],
        [
        "owner",
        "admin"
        ]
    ],
    "names": [
        "Plan001",
        "Plan002",
        "Plan003"
        ]
}
----


{sp} +

[[create-plan-def-catalog-http-response-codes]]
== HTTP response codes

[cols="1,3"]
|===
|Code |Description

|`201`
| success!

|`403`
| forbidden; user does not have appropriate privileges

|`404`
| plan definition catalog not found

|`500`
| internal SPCM error

|`503`
| request rejected due to overload

|===