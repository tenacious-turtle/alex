:context: update-prov-profile-api-operation
:api-object: provisioning profile
:api-op: postPut
= Update a provisioning profile

This `PUT` operation updates a provisioning profile. If successful, you receive an HTTP response code of `200` as well as a `JSON` response body showing the updated information passed in the <<update-prov-profile-json-payload, JSON request payload>>.

NOTE: See <<http-response-codes-{context}, HTTP response codes>> for other response codes.
 
===========================
[put]#*PUT*# ``/pcc/spcm/provisionProfile/<id>`` + <<update-prov-profile-json-payload, JSON request payload>>

*content-type*: application/JSON

*accept*: application/hal+JSON

*permissions*: `SPCM_PROVISION_PROFILE_UPDATE_PERMISSION`
===========================

== URL parameters

There is one mandatory parameter.

[cols="1,1,3"]
|===
| Parameter | Type | Description

|`id`
| integer
| The ID of the provisioning profile.

|===

[[update-prov-profile-json-payload]]
== JSON request payload

The following example shows a `JSON` payload for updating the `subscriberType` to `PREPAID`. Remember that the `id` of the provisioning profile is passed in the URL as a parameter.

TIP: See <<prov-profile-type-json-key-value-descriptions-{context}, provisioning profile type descriptions>> for descriptions.

[source,json]
----
{
    "locale": "en",
    "initialCorePlanName": "turkey",
    "subscriberClass": "gold",
    "subscriberType": "PREPAID",
    "dpsEnabled": false,
    "dpsNotification": false,
    "eosNotification": false,
    "paygNotification": false
}
----

// include the key value description table
include::./_prov-profile-type-json-key-value-descriptions.adoc[leveloffset=+2]

== JSON response

The following shows the response for <<update-prov-profile-json-payload, the above sample>>.

[source,json]
----
{
    "id": 39,
    "name": "turkeySauce",
    "defaultProfile": false,
    "locale": "en",
    "initialCorePlanName": "turkey",
    "subscriberClass": "gold",
    "subscriberType": "PREPAID",
    "dpsEnabled": false,
    "dpsNotification": false,
    "eosNotification": false,
    "paygNotification": false
}
----


{sp} +

include::../../general-api/_http-response-codes.adoc[leveloffset=+1]