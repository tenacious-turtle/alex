:context: update-counter-api-operation
:api-object: usage counter definition
:api-op: postPut
= Update a usage counter definition

This `PUT` operation updates the information for a specific usage counter definition by passing both `planDefinitionId` and `usageCounterDefinitionId` in the URL as well as a `JSON` payload to choosing what to update within the usage counter definition.

If successful, you receive an HTTP response code of `200` as well as a `JSON` response body showing the updated usage counter definition.

NOTE: See <<http-response-codes-{context}, HTTP response codes>> for other response codes.
 
===========================
[put]#*PUT*# [small]#``/pcc/spcm/planDefinitions/<planDefinitionId>/usageCounterDefinitions/<usageCounterDefinitionId>``# + <<update-counter-def-payload, JSON payload>>

*accept*: application/hal+JSON

*permissions*: `SPCM_PLAN_DEFINITION_CREATE_PERMISSION`
===========================

== URL parameters

There are two mandatory parameters.

[cols="1,1,3"]
|===
| Parameter | Type | Description

|`planDefinitionId`
| integer
| {define-planDefinitionId}

|`usageCounterDefinitionId`
| integer
| A unique identifier for the usage counter.

|===

{sp} +

[[update-counter-def-payload]]
== JSON request payload

The following example is updating a usage counter (`id` of `33`) with a new `unitMeteringType` setting (to `VOLUME`).

NOTE: See <<usage-counter-definition-type-json-key-value-descriptions-{context}, Plan definition descriptions>> for all possible data types that you might want to update.

[source,json]
----
{
    "id": 33,
    "name": "fairUsageCounter",
    "timeUnit": "NONE",
    "unitMeteringType": "VOLUME",
    "usageScope": "PLAN",
    "absoluteResetTime": "00:00:00",
    "_links": {
        "self": {
        "href": "http://localhost:8080/spcm-rest-ws/pcc/spcm/planDefinitions/
        167/usageCounterDefinitions/715"
        },
        "pccProfiles": {
        "href": "http://localhost:8080/spcm-rest-ws/pcc/spcm/planDefinitions/
        167/usageCounterDefinitions/715/pccProfiles"
        }
    }
}
----

== JSON response

The following response example confirms the update usage counter (`id` of `33`) with a new `unitMeteringType` setting (to `VOLUME`).

NOTE: See <<usage-counter-definition-type-json-key-value-descriptions-{context}, Plan definition descriptions>> for all possible data types that you might want to update.

[source,json]
----
{
    "id": 33,
    "name": "fairUsageCounter",
    "timeUnit": "NONE",
    "unitMeteringType": "VOLUME",
    "usageScope": "PLAN",
    "absoluteResetTime": "00:00:00",
    "_links": {
        "self": {
        "href": "http://localhost:8080/spcm-rest-ws/pcc/spcm/planDefinitions/
        167/usageCounterDefinitions/715"
        },
        "pccProfiles": {
        "href": "http://localhost:8080/spcm-rest-ws/pcc/spcm/planDefinitions/
        167/usageCounterDefinitions/715/pccProfiles"
        }
    }
}
----

{sp} +

// include the key value description table
include::./_usage-counter-definition-type-json-key-value-descriptions.adoc[leveloffset=+2]

include::../../general-api/_http-response-codes.adoc[leveloffset=+1]