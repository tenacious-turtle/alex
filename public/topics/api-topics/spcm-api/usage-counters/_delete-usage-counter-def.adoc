:context: delete-usage-counter-to-plan-definition-api-operation
:api-object: usage counter definition
:api-op: postPut
= Delete a usage counter definition

This `DELETE` operation deletes a specific usage counter definition by passing both `planDefinitionId` and `usageCounterDefinitionId` in the URL. If successful, you receive an HTTP response code of `204`.

NOTE: See <<http-response-codes-{context}, HTTP response codes>> for other response codes.
 
===========================
[delete]#*DELETE*# ``/pcc/spcm/planDefinitions/<planDefinitionId>/usageCounterDefinitions/<usageCounterDefinitionId>``

*permissions*: `SPCM_PLAN_DEFINITION_DELETE_PERMISSION`
===========================

== URL parameters

There are two mandatory parameters.

[cols="1,1,3"]
|===
| Parameter | Type | Description

|`planDefinitionId`
| integer
| {define-planDefinitionId}

|`usageCounterDefinitionId`
| integer
| A unique identifier for the usage counter.

|===

{sp} +

include::../../general-api/_http-response-codes.adoc[leveloffset=+1]