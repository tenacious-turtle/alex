:context: update-usage-rule-to-plan-definition-api-operation
:api-object: usage rule definition
:api-op: postPut
= Update a usage rule definition

This `PUT` operation updates a usage rule definition by using the `usageRuleDefinitionId` parameter as well as a `JSON` payload containing what you want to update. If successful, you receive an HTTP response code of `201` and a `JSON` response containing the updated usage rule definition information.

NOTE: See <<http-response-codes-{context}, HTTP response codes>> for other response codes.
 
===========================
[put]#*PUT*# [small]#``/pcc/spcm/planDefinitions/<planDefinitionId>/usageRuleDefinitions/<usageRuleDefinitionId>``# + <<update-usage-rule-json, JSON payload>>

*content-type*: application/hal+JSON

*accept*: application/hal+JSON

*permissions*: `SPCM_PLAN_DEFINITION_CREATE_PERMISSION`
===========================

== URL parameters

There are two mandatory parameters.

[cols="1,1,3"]
|===
| Parameter | Type | Description

|`planDefinitionId`
| integer
| {define-planDefinitionId}

|`usageRuleDefinitionId`
| integer
| The id of the usage rule definition.

|===

{sp} +

[[update-usage-rule-json]]
== JSON payload

This example updates a usage rule with an `id` of `42` and updates the `summary` to contain a bit more information regarding the rule.

[source,json]
----
{
    "id": 42,
    "name": "fairUsageRule",
    "summary": "this rule applies fair usage restraints",
    "threshold": 100,
    "maxDeactivationPeriod": null,
    "updateType": "NONE"
}
----

{sp} +

== JSON response

The following example shows a `JSON` response from the <<update-usage-rule-json, above JSON payload>>.

[source,json]
----
{
    "id": 42,
    "name": "fairUsageRule",
    "summary": "this rule applies fair usage restraints",
    "threshold": 100,
    "maxDeactivationPeriod": null,
    "updateType": "NONE"
    "_links": {
        "pccProfiles": {
            "href": "http://localhost:8080/spcm-rest-ws/pcc/spcm/planDefinitions/167/usageRuleDefinitions/42/pccProfiles"
        },
    "usageCounterDefinitions": {
        "href": "http://localhost:8080/spcm-rest-ws/pcc/spcm/planDefinitions/167/usageRuleDefinitions/42/usageCounterDefinition"
            },
    "self": {
        "href": "http://localhost:8080/spcm-rest-ws/pcc/spcm/planDefinitions/
        167/usageRuleDefinitions/42"
            }
        }
}
----

{sp} +

// include the key value description table
include::./_usage-rule-definition-type-json-key-value-descriptions.adoc[leveloffset=+2]

include::../../general-api/_http-response-codes.adoc[leveloffset=+1]