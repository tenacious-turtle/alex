:context: remove-action-from-usage-rule-definition-api-operation
:api-object: usage rule definition
:api-op: postPut
= Remove action from a usage rule definition

This `DELETE` operation removes the action from a usage rule definition. If successful, you receive an HTTP response code of `204`.

NOTE: See <<http-response-codes-{context}, HTTP response codes>> for other response codes.
 
===========================
[delete]#*DELETE*# ``/pcc/spcm/planDefinitions/<planDefinitionId>/usageRuleDefinitions/<usageRuleDefinitionId>/action``

*permissions*: `SPCM_PLAN_DEFINITION_DELETE_PERMISSION`
===========================

== URL parameters

There are are two mandatory parameters.

[cols="1,1,3"]
|===
| Parameter | Type | Description

|`planDefinitionId`
| integer
| {define-planDefinitionId}

|`usageRuleDefinitionId`
| integer
| The id of the usage rule definition.

|===

{sp} +

include::../../general-api/_http-response-codes.adoc[leveloffset=+1]