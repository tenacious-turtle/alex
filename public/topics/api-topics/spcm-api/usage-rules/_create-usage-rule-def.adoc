:context: create-usage-rule-to-plan-definition-api-operation
:api-object: usage rule definition
:api-op: postPut
= Create a usage rule definition

This `POST` operation adds a usage rule definition to an existing plan definition by using the `usageRuleDefinitions` endpoint as well as a `JSON` payload creating the usage rule. If successful, you receive an HTTP response code of `201` and a `JSON` response containing the new usage rule definition information.

NOTE: See <<http-response-codes-{context}, HTTP response codes>> for other response codes.
 
===========================
[post]#*POST*# ``/pcc/spcm/planDefinitions/<planDefinitionId>/usageRuleDefinitions + <<create-usage-rule-json, JSON payload>>``

*content-type*: application/hal+JSON

*accept*: application/hal+JSON

*permissions*: `SPCM_PLAN_DEFINITION_CREATE_PERMISSION`
===========================

== URL parameters

There are is one mandatory parameter.

[cols="1,1,3"]
|===
| Parameter | Type | Description

|`planDefinitionId`
| integer
| {define-planDefinitionId}

|===

{sp} +

[[create-usage-rule-json]]
== JSON payload

[source,json]
----
{
    "name": "fairUsageRule",
    "summary": null,
    "threshold": 100,
    "maxDeactivationPeriod": null,
    "updateType": "NONE"
}
----

{sp} +

== JSON response

The following example shows a `JSON` response when creating a new usage rule definition with the name `fairUsageRule`. Note the `id` of `730` which is auto-assigned.

[source,json]
----
{
    "id": 730,
    "name": "fairUsageRule",
    "summary": null,
    "threshold": 100,
    "maxDeactivationPeriod": null,
    "updateType": "NONE",
    "_links": {
        "pccProfiles": {
            "href": "http://localhost:8080/spcm-rest-ws/pcc/spcm/planDefinitions/167/usageRuleDefinitions/730/pccProfiles"
        },
    "usageCounterDefinitions": {
        "href": "http://localhost:8080/spcm-rest-ws/pcc/spcm/planDefinitions/167/usageRuleDefinitions/730/usageCounterDefinition"
            },
    "self": {
        "href": "http://localhost:8080/spcm-rest-ws/pcc/spcm/planDefinitions/
        167/usageRuleDefinitions/730"
            }
        }
}
----

{sp} +

// include the key value description table
include::./_usage-rule-definition-type-json-key-value-descriptions.adoc[leveloffset=+2]

include::../../general-api/_http-response-codes.adoc[leveloffset=+1]