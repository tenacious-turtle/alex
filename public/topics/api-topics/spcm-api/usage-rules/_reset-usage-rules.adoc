:context: reset-usage-rules-api-operation
:api-object: plan
:api-op: postPut
= Reset usage rules

This `PUT` operation resets the usage rules and counters for a plan. For example, the `state` of a rule that might be `violated` will reset to an `active` state. If successful, you receive an HTTP response code of `204`.

NOTE: See <<http-response-codes-{context}, HTTP response codes>> for other response codes.
 
===========================
[put]#*PUT*# ``/pcc/spcm/subscribers/<msisdn>/plans/<planId>/reset-usage``

*content-type*: application/json

*permissions*: `SPCM_PLAN_UPDATE_PERMISSION`
===========================

== URL parameters

There are two mandatory parameters.

[cols="1,1,3"]
|===
| Parameter | Type | Description

|`msisdn`
| string
| {define-msisdn}

|`planId`
| string
| The unique plan identifier.

|===

include::../../general-api/_http-response-codes.adoc[leveloffset=+1]
