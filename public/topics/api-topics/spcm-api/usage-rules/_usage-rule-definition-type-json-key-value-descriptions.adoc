= Usage rule definition type descriptions
[[usage-rule-definition-type-json-key-value-descriptions-{context}]]

[cols="2,1,3"]
|===
|Field |Type |Description

|`id`
| integer
a| The unique identifier for the usage rule. This is assigned by the server upon creation.

IMPORTANT: mandatory (__on update__)

|`name`
| string
a| The name of the usage rule.

IMPORTANT: mandatory

|`threshold`
| integer
a| The threshold (in bytes) at which the rule is violated.

IMPORTANT: mandatory

|`summary`
| string
| A summary of the usage rule definition.

|`maxDeactivationPeriod`
| string
a| The maximum period of time that which a subscriber can deactivate a usage rule. *Format*: `Xminute,Xhour,Xday,Xweek,Xmonth`

NOTE: mandatory if `updateType` is `ALL`.

|`updateType`
| enum
a| Specifies the update type.

.possible values
* `all` = rule can be updated
* `none` = rule cannot be updated

|===