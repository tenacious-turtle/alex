:context: get-pcc-profiles-for-usage-rule-to-plan-definition-api-operation
:api-object: usage rule definition
:api-op: getDelete
= Get pcc profiles for a usage rule definition

This `GET` operation retrieves all of the pcc profiles associated with a specific usage rule definition by passing the `usageRuleDefinitionId` parameter in the URL. If successful, you receive an HTTP response code of `200` and a `JSON` response containing the usage rule definition with its associated pcc profile(s).

NOTE: See <<http-response-codes-{context}, HTTP response codes>> for other response codes.
 
===========================
[get]#*GET*# ``/pcc/spcm/planDefinitions/<planDefinitionId>/usageRuleDefinitions/<usageRuleDefinitionId>/pccProfiles``

*accept*: application/hal+JSON

*permissions*: `SPCM_PLAN_DEFINITION_READ_PERMISSION`
===========================

== URL parameters

There are two mandatory parameters.

[cols="1,1,3"]
|===
| Parameter | Type | Description

|`planDefinitionId`
| integer
| {define-planDefinitionId}

|`usageRuleDefinitionId`
| integer
| The id of the usage rule definition.

|===

{sp} +

== JSON response

The following example shows a `JSON` response when retrieving a usage rule definition with the ``id`` `42` passed as `usageRuleDefinitionId` in the URL request. In the response, you can see the two pcc profiles of `755` and `756`.

NOTE: See <<pcc-profile-type-json-key-value-descriptions-{context},pcc profile type descriptions>> for more on what to expect to see in the pcc profiles.

[source,json]
----
{
    "_links": {
        "self": {
            "href": "http://localhost:8080/spcm-rest-ws/pcc/spcm/planDefinitions/167/usageRuleDefinitions/42/pccProfiles"
            }
    },
    "_embedded": {
        "pccProfiles": [
        {
            "id": 755,
            "alias": "DefaultProfile",
            "qosProfileName": "QoS Default",
            "serviceProfileName": "Service Default",
            "networkProfileName": "NetworkProfile1",
            "deviceProfileName": null,
            "timeProfileName": "TimeProfile1",
            "locationProfileName": "NorthMunster",
            "chargingProfileName": null,
            "subscriptionProfileName": null,
            "threshold": false,
            "meteringPercentage": null,
            "precedence": 1,
            "_links": {
                "self": {
                    "href": "http://localhost:8080/spcm-rest-ws/pcc/spcm/planDefinitions/167/pccProfiles/755"
                            }
                        }
                },
            {
            "id": 756,
            "alias": "DefaultProfile",
            "qosProfileName": "QoS Default",
            "serviceProfileName": "Service Default",
            "networkProfileName": "NetworkProfile1",
            "deviceProfileName": null,
            "timeProfileName": "TimeProfile1",
            "locationProfileName": null,
            "chargingProfileName": null,
            "subscriptionProfileName": null,
            "threshold": false,
            "meteringPercentage": null,
            "precedence": 1,
            "_links": {
                "self": {
                    "href": "http://localhost:8080/spcm-rest-ws/pcc/spcm/planDefinitions/167/pccProfiles/756"
                    }
                }
            }
        ]
    }
}
----

{sp} +

// include the key value description table
include::../plan-definitions/pcc-profiles/_pcc-profile-type-json-key-value-descriptions.adoc[leveloffset=+2]

include::../../general-api/_http-response-codes.adoc[leveloffset=+1]