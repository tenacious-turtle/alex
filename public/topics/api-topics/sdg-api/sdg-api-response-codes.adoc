* `0` = Success.
* `-1` = Failure.
* `-2` = The campaign already exists.
* `-3` = An link:https://www.oracle.com/industries/communications/products/services-gatekeeper/[OCSG, window="_blank"] connection failure occurred
* `-4` = Campaign expiration failure.
* `-5` = The campaign contains invalid dates.
* `-6` = The campaign does not exist.