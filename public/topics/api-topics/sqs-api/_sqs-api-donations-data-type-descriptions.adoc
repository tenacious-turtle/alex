= SQS donations data type descriptions
[[sqs-api-donations-data-type-descriptions-{context}]]

[cols="2,1,3"]
|===
|Field |Type |Description

|`id`
| string
| The __auto-assigned ID__ given to a donation upon creation.

|`donorId`
| string
a| The MSISDN of the donor subscriber.

NOTE: {define-msisdn}

|`donorPlanId`
| string
a| The ID of the donor's shareable plan.

|`quotaType`
| string
a| Specifies the quota type. This is `share` or `amount`.

NOTE: ``share`` is a percentage of quota and `amount` is a specific amount (whether that is data, time, or credit). The type itself is determined from the donor plan definition's `unitMeteringType`.

|`recipients`
| array
| An array that lists recipients of the donation. Recipients will have a `recipientId` and a `quota` which determines how much is shared with them.

|`recipientId`
| integer
a| The unique ID for the recipient. This is typically the MSISDN.

NOTE: {define-msisdn}

|`quota`
| integer
| The quota being shared with the recipient.

|`groupId`
| string
| The unique ID of the group. Group IDs are auto-assigned upon creation.

|===
