:context: delete-rec-don-sqs-api-operation
:api-object: recurring donation
:api-op: getDelete
= Delete a recurring donation

This `DELETE` operation deletes a recurring donation by `id`. If successful, you receive an HTTP response code of `204`.

NOTE: See <<http-response-codes-{context}, HTTP response codes>> for other response codes.

===========================
[delete]#*DELETE*# ``/sqs/api/recurringDonations/<id>``

*authorisation*: link:https://en.wikipedia.org/wiki/Basic_access_authentication[HTTP Basic Auth, window="_blank"]

*permissions*: `SQS_RECURRING_DONATION_DELETE_PERMISSION`
===========================

{sp} +

== URL parameters

[cols="1,3"]
|===
|Parameter |Description

|`id`
| The auto-assigned identifier that was given to the recurring donation upon creation.

|===

include::../../general-api/_http-response-codes.adoc[leveloffset=+1]