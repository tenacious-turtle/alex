:context: create-rec-don-sqs-api-operation
:api-object: recurring donation
:api-op: postPut
= Create a recurring donation
[[create-rec-donation]]

This `POST` operation creates a recurring donation by passing the donorPlanId and groupId in a `JSON` payload. If successful, you receive an HTTP response code of `201` and a `JSON` response outlining the information passed in the <<sqs-create-rec-don-json-payload, JSON request payload>> along with an auto-created `id`.

======
IMPORTANT: This operation configures a donation to be executed upon plan renewal, but does not actually donate anything at the time of the request.

NOTE: Only 1 recurring donation is allowed per donor plan and donor id. The `donorId` is set to the group owner automatically.
======

NOTE: See <<http-response-codes-{context}, HTTP response codes>> for other response codes.

===========================
[post]#*POST*# ``/sqs/api/recurringDonations`` + <<sqs-create-rec-don-json-payload, JSON payload>>

*content-type*: application/JSON

*accept*: application/JSON

*authorisation*: link:https://en.wikipedia.org/wiki/Basic_access_authentication[HTTP Basic Auth, window="_blank"]

*permissions*: `SQS_RECURRING_DONATION_CREATE_UPDATE_PERMISSION`
===========================

[[sqs-create-rec-don-json-payload]]
== JSON request payload

You pass the `donorPlanId` and `groupId` in the `JSON` payload.

IMPORTANT: The *owner ID|name pair* must be unique as an owner cannot have two groups with the same name. 

[source,json]
----
{
   "donorPlanId" : 123,
   "groupId" : "ASDS"
}
----

// include the key value description table
// include::./_donation-type-json-key-value-descriptions.adoc[leveloffset=+2]

{sp} +

[[sqs-create-rec-don-json-response]]
== JSON response

The `JSON` response includes the information passed in the <<sqs-create-rec-don-json-payload, JSON payload>> with an auto-assigned `id`.

NOTE: See xref:sqs-api-recurring-donations-data-type-descriptions-{context}[recurring donation data type descriptions] for more details on the fields in the JSON request and response.

[source,json]
----
{
   "id" : "E5B412",
   "donorPlanId" : 123,
   "groupId" : "ASDS",
   "donorId" : "4564563",
   "planName" : "SharePlan",
   "created": "2019-08-07T15:01:58.000+0000",
   "updated": "2019-08-07T15:01:58.000+0000"
}
----

// include the key value description table
include::./_sqs-api-recurring-data-type-descriptions.adoc[leveloffset=+2]

include::../../general-api/_http-response-codes.adoc[leveloffset=+1]
