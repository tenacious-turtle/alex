:context: get-rec-don-sqs-api-operation
:api-object: recurring donation
:api-op: getDelete
= Get a recurring donation

This `GET` operation retrieves a recurring donation. If successful, you receive an HTTP response code of `200` and a `JSON` response containing the recurring donation information.

NOTE: See <<http-response-codes-{context}, HTTP response codes>> for other response codes.

===========================
[get]#*GET*# ``/sqs/api/recurringDonations/<id>``

*accept*: application/JSON

*authorisation*: link:https://en.wikipedia.org/wiki/Basic_access_authentication[HTTP Basic Auth, window="_blank"]

*permissions*: `SQS_RECURRING_DONATION_READ_PERMISSION`
===========================

{sp} +

== URL parameters

[cols="1,3"]
|===
|Parameter |Description

|`id`
| The auto-assigned identifier that was given to the recurring donation upon creation.

|===

// include the key value description table
// include::./_donation-type-json-key-value-descriptions.adoc[leveloffset=+2]

{sp} +

[[sqs-get-rec-don-json-response]]
== JSON response

The `JSON` response includes the information about the recurring donation including the `donorId` and `groupId`.

NOTE: See xref:sqs-api-recurring-donations-data-type-descriptions-{context}[recurring donation data type descriptions] for more details on the fields in the JSON request and response.

[source,json]
----
{
    "id": "E5B412",
    "donorId": "0871234567",
    "donorPlanId": 11224455,
    "groupId": "ABC123",
    "created": "2019-08-07T15:01:58.000+0000",
    "updated": "2019-08-07T15:01:58.000+0000"
}
----

// include the key value description table
include::./_sqs-api-recurring-data-type-descriptions.adoc[leveloffset=+2]


include::../../general-api/_http-response-codes.adoc[leveloffset=+1]
