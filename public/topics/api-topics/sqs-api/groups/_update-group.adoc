:context: update-group-sqs-api-operation
:api-object: group
:api-op: getDelete
= Update a group

This `PUT` operation updates group information by passing the `id` in the URL and a `JSON` request with the updated information. If successful, you receive an HTTP response code of `200` and a `JSON` response outlining the updated information passed in the <<sqs-update-group-json-payload, JSON request payload>>.


NOTE: See <<http-response-codes-{context}, HTTP response codes>> for other response codes.

===========================
[put]#*PUT*# ``/sqs/api/groups/<id>`` + <<sqs-update-group-json-payload, JSON request payload>>

*content-type*: application/JSON

*accept*: application/JSON

*authorisation*: link:https://en.wikipedia.org/wiki/Basic_access_authentication[HTTP Basic Auth, window="_blank"]

*permissions*: `SQS_GROUP_CREATE_UPDATE_PERMISSION`
===========================

{sp} +

== URL parameters

[cols="1,3"]
|===
|Parameter |Description

|`id`
| The auto-assigned identifier that was given to the group upon creation.

|===

[[sqs-update-group-json-payload]]
== JSON request payload

This sample updates the group `name` to `CherokeeNation`.

NOTE: You must include all of the key:value pairs shown below in order to update a group.

[source,json]
----
{
  "id": "By5li9cbFR53VLv6fh6z",
  "ownerId": "967178860",
  "name" : "CherokeeNation"
}
----

// include the key value description table
// include::./_donation-type-json-key-value-descriptions.adoc[leveloffset=+2]

{sp} +

[[sqs-update-group-json-response]]
== JSON response

The `JSON` response includes the updated information passed in the <<sqs-update-group-json-payload, JSON payload>>.

[source,json]
----
{
    "created": "2020-01-20T13:43:01.000+0000",
    "updated": "2020-01-20T13:43:01.000+0000",
    "id": "By5li9cbFR53VLv6fh6z",
    "ownerId": "967178860",
    "name": "CherokeeNation"
}
----

// include the key value description table
include::./_sqs-api-groups-data-type-descriptions.adoc[leveloffset=+2]

include::../../general-api/_http-response-codes.adoc[leveloffset=+1]
