= SQS members data type descriptions
[[sqs-api-members-data-type-descriptions-{context}]]

[cols="2,1,3"]
|===
|Field |Type |Description

|`memberId`
| string
a| The unique ID for the member. This is typically the MSISDN of the subscriber.

NOTE: {define-msisdn}

|`quota`
| integer
a| The quota amount shared with the specified group member. This can be an exact amount or most often, a percentage of the donor plan.

NOTE: Quota can be volume of data, time, or credit. This is all determined by the `unitMeteringType` in the donor plan definition.

|===
