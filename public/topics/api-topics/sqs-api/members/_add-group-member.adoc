:context: add-member-sqs-api-operation
:api-object: group member
:api-op: postPut
= Add member to a group
[[sqs-add-member-{guide-slug}]]

This `PUT` operation adds as well as updates a member of a group by passing first the group's ID then the ``memberId`` into the URL along with a `JSON` payload defining how much of the data quota of the group is to be shared with the member. If successful, you receive an HTTP response code of `201` and a `JSON` response outlining the information passed in the <<sqs-add-member-json-payload, JSON request payload>>.

IMPORTANT: The percentage of quota shared is expressed as an integer between `0` and `10000000`.  So for example, 5% is 5/100 * 10000000 which equals ``500000`` which would be what you specify in the payload. When sharing with a group, the specified shared quota is the amount given to each member in the group. So, if you specify 10% of 1GB (100MB), each member gets this amount.

NOTE: See <<http-response-codes-{context}, HTTP response codes>> for other response codes.

===========================
[put]#*PUT*# ``/sqs/api/groups/<id>/members/<memberId>`` + <<sqs-add-member-json-payload, JSON request payload>>

*content-type*: application/JSON

*accept*: application/JSON

*authorisation*: link:https://en.wikipedia.org/wiki/Basic_access_authentication[HTTP Basic Auth, window="_blank"]

*permissions*: `SQS_GROUP_CREATE_UPDATE_PERMISSION`
===========================

== URL parameters

[cols="1,3"]
|===
|Parameter |Description

|`id`
| The auto-assigned identifier that was given to the group upon creation.

|`memberId`
| A unique identifier for the member.

|===

[[sqs-add-member-json-payload]]
== JSON request payload

You must pass the `memberId` in the `JSON` payload as well as the URL plus the quota.

[source,json]
----
{
   "memberId":"678678",
   "quota":123456
}
----

// include the key value description table
include::./_sqs-api-members-data-type-descriptions.adoc[leveloffset=+2]

{sp} +

[[sqs-add-member-json-response]]
== JSON response

The `JSON` response includes the information passed in the <<sqs-add-member-json-payload, JSON payload>> as confirmation.

[source,json]
----
{
   "memberId":"678678",
   "quota":123456
}
----

include::../../general-api/_http-response-codes.adoc[leveloffset=+1]

:context: sqs-api-guide
